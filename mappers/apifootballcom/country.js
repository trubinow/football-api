class Country {
  constructor({
    country_id = null,
    country_name = '',
    image = ''
  } = {}) {
    this.id = country_id;
    this.name = country_name;
    this.image = image;
  }
}

module.exports = Country;
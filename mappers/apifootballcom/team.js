class Team {
  constructor({
    team_name,
    league_id = null,
    country_id = null,
    country_name = '',
    image = ''
  } = {}) {
    //this.id = league_id;
    this.name = team_name;

    this.countryId = country_id;
    this.countryName = country_name;

    this.leagueId = league_id;
    this.image = image;
  }
}

module.exports = Team;
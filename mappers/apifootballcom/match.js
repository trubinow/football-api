class Match {
  constructor({
    match_id = null,

    country_id = null,
    country_name = '',

    league_id = null,
    league_name = '',

    match_date = null,
    match_time = null,
    match_status = '',

    match_hometeam_name = '',
    match_hometeam_score = '',

    match_awayteam_name = '',
    match_awayteam_score = '',

    goalscorer = [],
    cards = [],
    lineup = [],
    statistics = [],

    match_live = false

  } = {}) {

    this.id = match_id;

    this.countryId = country_id;
    this.countryName = country_name;

    this.leagueId = league_id;
    this.leagueName = league_name;

    this.date = match_date;
    this.time = match_time;
    this.status = match_status;

    this.homeTeamName = match_hometeam_name;
    this.homeTeamScore = match_hometeam_score;

    this.awayTeamName = match_awayteam_name;
    this.awayTeamScore = match_awayteam_score;

    this.goalScorer = goalscorer,
    this.cards = cards,
    this.lineup = lineup,
    this.statistics = statistics,
    this.live = match_live;
  }
}

module.exports = Match;
class League {
  constructor({
    league_id = null,
    league_name = '',
    country_id = null,
    country_name = '',
    teams = [],
    image = ''
  } = {}) {
    this.id = league_id;
    this.name = league_name;

    this.countryId = country_id,
    this.countryName = country_name,
    this.teams = [];
    this.image = image;
  }
}

module.exports = League;
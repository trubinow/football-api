const config = require('config');
const express = require('express');
const logger = require('morgan');

const app = express();
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

app.use(logger('short'));

if (process.env.NODE_ENV == 'development') {

  const passport = require('passport');
  require('./other/passport');

  app.use(passport.initialize());

  app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');

    next();
  });
}

const utility = require('./other/utility');
const index = require('./routes/v1/index');

app.use(utility.mw);
app.use('/api', index);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use(function (err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401);
    res.json({"message" : err.name + ": " + err.message});
  } else next(err);
});

// error handler
app.use(function(err, req, res) {

  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json({error: err.message});
});

utility.matchesUpdater(config.get('matchesUpdater.interval'));

module.exports = app;
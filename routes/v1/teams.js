const express = require('express');
const teamsRouter = express.Router();
const errorMsg = require('debug')('route::teams:error');

const RespFormat = require('./../../models/topLevel');

teamsRouter.get('/assets', (req, res) => {

  let page = (req.query.page && parseInt(req.query.page) == req.query.page && req.query.page > 0) ? parseInt(req.query.page) : 1;
  let perPage = (req.query.perPage && parseInt(req.query.perPage) == req.query.perPage && req.query.perPage > 0) ? parseInt(req.query.perPage) : 200;
  let filter = {};

  req.matchProxy.getAssets(filter, {page: page, perPage: perPage})
    .then((result) => {

      if (Array.isArray(result) && result.length > 0) {
        result = result
          .reduce((aggr, item, index) => {

            if (item.homeTeamId && item.homeImageBase) {
              aggr.push({id: item.homeTeamId, imageBase: item.homeImageBase});
            }

            if (item.awayTeamId && item.awayImageBase) {
              aggr.push({id: item.awayTeamId, imageBase: item.awayImageBase});
            }

            return aggr;

          }, [])
      }

      res.status(200).json(new RespFormat({
        data: result || [],
        page: page,
        itemsPerPage: perPage,
        method: 'teams/assets',
        params: [{filter: filter}]
      }));
    })
    .catch((error) => {
      errorMsg(error.message);
      res.status(500).json({errors: ['Internal server error.']});
    });
});

module.exports = teamsRouter;
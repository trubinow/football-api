const express = require('express');
const passport = require('passport');

const User = require('../../../mongo/models/Account');

const usersRouter = express.Router();

usersRouter.post('/register', (req, res) => {

  if (!req.body.username || !req.body.password) {
    res.status(200).json({method: 'register', result: '0', message: 'All fields are required'});

    return;
  }

  let user = new User();

  user.username = req.body.username;
  user.setPassword(req.body.password);
  user.active = '0';

  user.save(function(err){

    if (err) {
      res.status(200).json({method: 'register', result: '0', message: err.message});
    } else {
      let token = user.generateJwt();
      res.status(200).json({method: 'register', result: '1', token: token});
    }
  })

});

usersRouter.post('/login', (req, res, next) => {

  if (!req.body.username || !req.body.password) {
    res.status(200).json({method: 'login', result: '0', message: 'All fields are required'});

    return;
  }

  passport.authenticate('local', function(err, user, info){

    if (err) {
      res.status(200).json({method: 'login', result: '0', message: err.message});
      return;
    }

    if (user) {
      let token = user.generateJwt();
      res.status(200).json({method: 'login', result: '1', token: token});
    } else {
      res.status(401).json({method: 'login', result: '1', message: info});
    }

  })(req, res, next);
});

usersRouter.get('/logout', (req, res) => {
  req.logout();
  res.status(200).json({method: 'logout'});
});

module.exports = usersRouter;
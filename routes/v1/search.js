const express      = require('express');
const searchRouter = express.Router();
const errorMessage = require('debug')('search:error');

searchRouter.get('/:keyword', (req, res) => {

  let keyword = req.params.keyword;

  req.searchProxy.get(keyword)
    .then((result) => {
      if (result) {
        res.status(200).json(result);
      } else {
        res.status(204).json([]);
      }
    })
    .catch((error) => {
      errorMessage(error.message);
      res.status(500).json({errors: ['Internal server error.']});
    });
});

module.exports = searchRouter;
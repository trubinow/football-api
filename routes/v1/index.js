const express = require('express');

const countries = require('./countries');
const leagues = require('./leagues');
const teams = require('./teams');
const matches = require('./matches');
const populate = require('./populate');

const router = express.Router();

/* GET countries-list. */
router.use('/countries', countries);

/* GET leagues-list. */
router.use('/leagues', leagues);

/* GET teams-list. */
router.use('/teams', teams);

/* GET matches-list. */
router.use('/matches', matches);

/* GET search. */
//router.use('/search', search);

/*Populate*/
router.use('/populate', populate);

if (process.env.NODE_ENV == 'development') {
  const admin = require('./admin');
  const user = require('./users');

  const jwt = require('express-jwt');
  const auth = jwt({
    secret: process.env.JWT_SECRET,
    userProperty: 'payload'
  });

  /*Admin*/
  router.use('/admin', auth, admin);

  /*Users*/
  router.use('/user', user);
}

module.exports = router;
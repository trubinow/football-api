const express = require('express');
const countriesRouter = express.Router();
const errorMsg = require('debug')('route::countries:error');

const RespFormat = require('./../../models/topLevel');

countriesRouter.get('/', (req, res) => {

  let page = (req.query.page && parseInt(req.query.page) == req.query.page && req.query.page > 0) ? parseInt(req.query.page) : 1;
  let perPage = (req.query.perPage && parseInt(req.query.perPage) == req.query.perPage && req.query.perPage > 0) ? parseInt(req.query.perPage) : 200;
  let filter = {};

  req.countryProxy.getListAdmin(filter, {page: page, perPage: perPage})
    .then((result) => {
      res.status(200).json(new RespFormat({
        data: result || [],
        page: page,
        itemsPerPage: perPage,
        method: 'countries',
        params: [{filter: filter}]
      }));
    })
    .catch((error) => {
      errorMsg(error.message);
      res.status(500).json({errors: ['Internal server error.']});
    });

});

countriesRouter.get('/:countryId/leagues', (req, res) => {
  let countryId = req.params.countryId;

  req.leagueProxy.getByCountryId(countryId)
    .then((result) => {
      res.status(200).json(
        new RespFormat({data: result || [], method: 'countries/:countryId/leagues', params: [{countryId: countryId}]})
      );
    })
    .catch((error) => {
      errorMsg(error.message);
      res.status(500).json({errors: ['Internal server error.']});
    });
});

module.exports = countriesRouter;
const express = require('express');
const errorMsg = require('debug')('routes::leagues:error');
const leaguesRouter = express.Router();
const RespFormat = require('./../../models/topLevel');

leaguesRouter.get('/:leagueId/teams', (req, res) => {

  let leagueId = req.params.leagueId;

  req.leagueProxy.getTeams(leagueId)
    .then((result) => {
      res.status(200).json(new RespFormat({data: result || [], method: 'leagues/:leagueId/teams', params: [{leagueId: leagueId}]}));
    })
    .catch((error) => {
      errorMsg(error.message);
      res.status(500).json({errors: ['Internal server error.']});
    });

});

module.exports = leaguesRouter;
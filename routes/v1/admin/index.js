const express = require('express');

const countries = require('./countries');
const teams = require('./teams');
const leagues = require('./leagues');
const search = require('./search');

const router = express.Router();

/* GET countries-list. */
router.use('/countries', countries);

/* GET teams-list. */
router.use('/teams', teams);

/* GET leagues-list. */
router.use('/leagues', leagues);

/* GET search-list. */
router.use('/search', search);


module.exports = router;
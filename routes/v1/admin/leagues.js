const express = require('express');
const infoMsg = require('debug')('routes::admin:info');
const errorMsg = require('debug')('routes::admin:error');
const leaguesRouter = express.Router();
const RespFormat = require('./../../../models/topLevel');


leaguesRouter.get('/', (req, res) => {

  let page = (req.query.page && parseInt(req.query.page) == req.query.page && req.query.page > 0) ? parseInt(req.query.page) : 1;
  let perPage = (req.query.perPage && parseInt(req.query.perPage) == req.query.perPage && req.query.perPage > 0) ? parseInt(req.query.perPage) : 20;
  let filter = req.query.filter || '{}';

  try {
    filter = typeof (JSON.parse(filter)) == 'object' ? JSON.parse(filter) : {};

    req.leagueProxy.getList(filter, {page: page, perPage: perPage})
      .then((result) => {

        res.status(200).json(new RespFormat({
          data: result || [],
          page: page,
          itemsPerPage: perPage,
          method: 'leagues',
          params: [{filter: filter}],
          total: 1348
        }));
      })
      .catch((error) => {
        errorMsg(error.message);
        res.status(500).json({errors: ['Internal server error.']});
      });

  }  catch(e) {
    errorMsg(e.message);
    res.status(200).json(new RespFormat({data: [], method: 'leagues', params: [{}], itemsPerPage: perPage, total: 0}));
  }
});

leaguesRouter.get('/:id', (req, res) => {

  req.leagueProxy.getList({id: req.params.id})
    .then((result) => {
      res.status(200).json(new RespFormat({data: result || [], method: 'leagues', params: []}));
    })
    .catch((error) => {
      errorMsg(error.message);
      res.status(500).json({errors: ['Internal server error.']});
    });
});

leaguesRouter.put('/:id', (req, res) => {

  req.leagueProxy.save(req.body)
    .then((result) => {
      res.status(200).json(new RespFormat({data: result || [], method: 'leagues', params: []}));
    })
    .catch((error) => {
      errorMsg(error.message);
      res.status(500).json({errors: ['Internal server error.']});
    });

});

module.exports = leaguesRouter;
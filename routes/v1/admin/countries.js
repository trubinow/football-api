const express = require('express');

const infoMsg = require('debug')('routes::admin:countries:info');
const errorMsg = require('debug')('routes::admin:countries:error');

const countriesRouter = express.Router();
const RespFormat = require('./../../../models/topLevel');

countriesRouter.get('/', (req, res) => {

  let page = (req.query.page && parseInt(req.query.page) == req.query.page && req.query.page > 0) ? parseInt(req.query.page) : 1;
  let perPage = (req.query.perPage && parseInt(req.query.perPage) == req.query.perPage && req.query.perPage > 0) ? parseInt(req.query.perPage) : 20;
  let filter = req.query.filter || '{}';

  try {
    filter = typeof (JSON.parse(filter)) == 'object' ? JSON.parse(filter) : {};

    req.countryProxy.getListAdmin(filter, {page: page, perPage: perPage})
      .then((result) => {
        res.status(200).json(new RespFormat({
          data: result || [],
          page: page,
          itemsPerPage: perPage,
          method: 'countries',
          params: [{filter: filter}],
          total: 106
        }));
      })
      .catch((error) => {
        errorMsg(error.message);
        res.status(500).json({errors: ['Internal server error.']});
      });

  }  catch(e) {
    errorMsg(e.message);
    res.status(200).json(new RespFormat({data: [] || [], method: 'countries', params: [{}], itemsPerPage: perPage, total: 0}));
  }
});

countriesRouter.get('/:id', (req, res) => {

  req.countryProxy.getListAdmin({id: req.params.id})
    .then((result) => {
      res.status(200).json(new RespFormat({data: result || [], method: 'countries', params: []}));
    })
    .catch((error) => {
      errorMsg(error.message);
      res.status(500).json({errors: ['Internal server error.']});
    });
});

countriesRouter.put('/:id', (req, res) => {

  req.countryProxy.save(req.body)
    .then((result) => {
      res.status(200).json(new RespFormat({data: result || [], method: 'countries', params: []}));
    })
    .catch((error) => {
      errorMsg(error.message);
      res.status(500).json({errors: ['Internal server error.']});
    });
});

module.exports = countriesRouter;
const express = require('express');
const infoMsg = require('debug')('routes::admin:info');
const errorMsg = require('debug')('routes::admin:error');
const teamsRouter = express.Router();
const RespFormat = require('./../../../models/topLevel');

const url = require('url');
const {download, deleteAll} = require('./../../../other/download');
const decode = require('./../../../other/decode');

teamsRouter.get('/', (req, res) => {

  let page = (req.query.page && parseInt(req.query.page) == req.query.page && req.query.page > 0) ? parseInt(req.query.page) : 1;
  let perPage = (req.query.perPage && parseInt(req.query.perPage) == req.query.perPage && req.query.perPage > 0) ? parseInt(req.query.perPage) : 20;
  let filter = req.query.filter || '{}';

  try {

    filter = typeof (JSON.parse(filter)) == 'object' ? JSON.parse(filter) : {};

    let promiseArr = [ req.teamProxy.getList(filter, {page: page, perPage: perPage}), req.teamProxy.getCount(filter)];
    Promise.all(promiseArr)
      .then((result) => {
        res.status(200).json(new RespFormat({
          data: result[0] || [],
          total: result[1] || 0,
          page: page,
          itemsPerPage: perPage,
          method: 'teams',
          params: [{filter: filter}]
        }))
      })
      .catch((error) => {
        errorMsg(error.message);
        res.status(500).json({errors: ['Internal server error.']});
      });

  }  catch(e) {
    errorMsg(e.message);
    res.status(200).json(new RespFormat({data: [] || [], method: 'teams', params: [{}], itemsPerPage: perPage, total: 0}));
  }
});

teamsRouter.get('/:id', (req, res) => {

  req.teamProxy.getList({id: req.params.id}, {page :1, perPage: 1}, true)
    .then((result) => {
      res.status(200).json(new RespFormat({data: result || [], method: 'teams', params: []}));
    })
    .catch((error) => {
      errorMsg(error.message);
      res.status(500).json({errors: ['Internal server error.']});
    });
});

teamsRouter.put('/:id', (req, res) => {

  new Promise((resolve, reject) => {

    if (!req.body.image) {
      resolve(false)
    } else {

      let imgUrl = 'http:' + req.body.image.replace(/[0-9]{1,3}px-/, 64 + 'px-');
      let filename = url.parse(imgUrl).pathname.split('/').pop();

      filename = `../sport/soccer/${filename}`;

      resolve({filename: filename, imgUrl: imgUrl});
    }
  })
    .then((result) => {
      if (result) {
        return download(result.imgUrl, req.body.id, result.filename);
      } else {
        return false;
      }
    })
    .then(result => {
      if (result)
        return decode(result)
      else
        return false;
    })
    .then(result => {

      if (result && result.imageBase) {
        req.body.imageBase = result.imageBase;
      } else {
        req.body.imageBase = '';
      }

      return req.teamProxy.save(req.body)
    })
    .then((result) => {
      res.status(200).json(new RespFormat({data: result || [], method: 'teams', params: []}));
    })
    .catch((error) => {
      errorMsg(error.message);
      res.status(500).json({errors: ['Internal server error.']});
    });


});

module.exports = teamsRouter;
const express = require('express');
const infoMsg = require('debug')('routes::admin:search:info');
const errorMsg = require('debug')('routes::admin:search:error');
const searchRouter = express.Router();
const RespFormat = require('./../../../models/topLevel');

searchRouter.get('/teams/:keyword', (req, res) => {

  let page = (req.query.page && parseInt(req.query.page) == req.query.page && req.query.page > 0) ? parseInt(req.query.page) : 1;
  let perPage = (req.query.perPage && parseInt(req.query.perPage) == req.query.perPage && req.query.perPage > 0) ? parseInt(req.query.perPage) : 20;
  let filter = {name: new RegExp(req.params.keyword, 'i')};

  let promiseArr = [ req.teamProxy.getList(filter, {page: page, perPage: perPage}), req.teamProxy.getCount(filter)];

  Promise.all(promiseArr)
    .then((result) => {
      res.status(200).json(new RespFormat({
        data: result[0] || [],
        total: result[1] || 0,
        page: page,
        itemsPerPage: perPage,
        method: 'search/teams',
        params: []}));
    })
    .catch((error) => {
      errorMsg(error.message);
      res.status(500).json({errors: ['Internal server error.']});
    });
});

module.exports = searchRouter;
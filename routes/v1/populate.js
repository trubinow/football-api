const express = require('express');
const populateRouter = express.Router();

const config = require('config');
const limiterConfig = config.get('limiter');
const redisConfig = config.get('redis');

const LimiterClass = require('../../services/limiter');
const ParserClass = require('../../services/parser');

const limiter = new LimiterClass(limiterConfig.reqPerTenSeconds, limiterConfig.queueId, redisConfig);
const parser = new ParserClass(limiter);

populateRouter.get('/', (req, res) => {
  res.status(404).send('NOT FOUND');
});

populateRouter.post('/countries', (req, res) => {
  req.countriesPopulateService.run();
  res.status(200).json({populate: 'countries'});
});

populateRouter.post('/leagues', (req, res) => {
  req.leaguesPopulateService.run();
  res.status(200).json({populate: 'leagues'});
});

populateRouter.post('/teams', (req, res) => {
  req.teamsPopulateService.run();
  res.status(200).json({populate: 'teams'});
});

populateRouter.post('/teams/imagesImport', (req, res) => {

  let step = req.body.step && parseInt(req.body.step) == req.body.step ? parseInt(req.body.step) : 1;
  let perPage = req.body.perPage && parseInt(req.body.perPage) == req.body.perPage ? parseInt(req.body.perPage) : 1000;

  req.teamsPopulateService.runImagesImport({image: {$in: [null, '']}, step: step }, {page: 1, perPage: perPage}, step);
  res.status(200).json({populate: 'teamsLogos'});
});

populateRouter.post('/teams/imagesDecode', (req, res) => {
  let perPage = req.body.perPage && parseInt(req.body.perPage) == req.body.perPage ? parseInt(req.body.perPage) : 1000;
  req.teamsPopulateService.runImagesDecode({imageBase: null, image: {$not: {$eq: ''}}}, {page : 1, perPage: perPage});

  res.status(200).json({populate: 'teamsBase64'});
});

populateRouter.post('/countries/imagesImport', (req, res) => {
  req.countriesPopulateService.runImagesImport();
  res.status(200).json({populate: 'countriesFlags'});
});

populateRouter.post('/leagues/imagesImport', (req, res) => {
  req.leaguesPopulateService.runImagesImport();
  res.status(200).json({populate: 'leaguesLogos'});
});

//leagueId: 63
populateRouter.post('/matches', (req, res) => {
  req.matchesPopulateService.run();
  res.status(200).json({populate: 'matches'});
});

populateRouter.get('/wiki', (req, res) => {
  //req.matchesPopulateService.run();
  let query = req.query.q && req.query.q.length > 3 ? req.query.q : false;

  if (query !== false) {

    req.teamProxy.getVariants(query)
      .then((data) => {
        res.status(200).json({populate: 'wiki', query: query, variants: data});
      })
  } else
    res.status(200).json({populate: 'wiki', query: query});
});

module.exports = populateRouter;
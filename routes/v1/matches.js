const express = require('express');
const config = require('config');

const matchesRouter = express.Router();
const errorMsg = require('debug')('routes::leagues:error');
const RespFormat = require('./../../models/topLevel');

matchesRouter.get('/', (req, res) => {

  let page = (req.query.page && parseInt(req.query.page) == req.query.page && req.query.page > 0) ? parseInt(req.query.page) : 1;
  let perPage = (req.query.perPage && parseInt(req.query.perPage) == req.query.perPage && req.query.perPage > 0) ? parseInt(req.query.perPage) : config.get('pagination.matches.itemsPerPage');
  let filter = req.query.filter || '{}';

  try {
    filter = typeof (JSON.parse(filter)) == 'object' ? JSON.parse(filter): {};

    let allowed = ["date", "live"];
    let query = {};

    allowed.forEach((item) => {
      if (item in filter) {
        query[item] = filter[item].toString();
      }
    })

    req.matchProxy.getList(query, {page: page, perPage: perPage})
        .then((result) => {
        res.status(200).json(new RespFormat({data: result || [], method: 'matches', params: [{filter: query}], page: page, itemsPerPage: perPage}));
      })
      .catch((error) => {
        errorMsg(error.message);
        res.status(500).json({errors: ['Internal server error.']});
      });

  } catch(e) {
    errorMsg(e.message);
    res.status(200).json(new RespFormat({data: [], method: 'matches', params: [{}], itemsPerPage: perPage}));
  }

});

module.exports = matchesRouter;
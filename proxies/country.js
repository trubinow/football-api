const infoMessage  = require('debug')('country-proxy:info');
const errorMessage = require('debug')('country-proxy:error');

const cheerio = require('cheerio');
const wikiProvider = require('../providers/wikipedia');

class CountryProxy {
  constructor(mongoRepo, apiRepo) {
    this.mongoRepo = mongoRepo;
    this.apiRepo = apiRepo;
  }

  save(item) {
    return this.mongoRepo.save(item);
  }

  /**
   * @returns {Promise<[Country]>}
   */
  getList() {
    return new Promise((resolve, reject) => {
      this.mongoRepo.getList()
        .then((models) => {
          resolve(models);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  /**
   * @returns {Promise<[Country]>}
   */
  getListAdmin(filter = {}, pagination = {page :1, perPage: 50}) {
    return new Promise((resolve, reject) => {
      this.mongoRepo.getListAdmin(filter, pagination)
        .then((models) => {
          resolve(models);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  getImages(filter = {}, pagination = {page :1, perPage: 50}) {

    let countries = [];

    return this.mongoRepo.getList(filter, pagination)
      .then((models) => {

        models.forEach((item) => {

          if (item.name && item.image == '') {

            countries.push(
              new Promise((resolve, reject) => {

                let url = wikiProvider.countries(item.name);

                this.apiRepo.api.getData(url)
                  .then((body) => {

                    const $ = cheerio.load(body);
                    const imgArr = $('table.infobox img');
                    const src = imgArr.length > 0 && 'attribs' in imgArr[0] ? imgArr[0].attribs.src : null;

                    if (src) {
                      item.image = src;
                      infoMessage(`Logo obtained for country ${item.name} -- ${src}`);
                    }

                    this.mongoRepo.save(item)
                      .then((res) => {
                        infoMessage('Saved');
                        resolve(true);
                      })
                      .catch((err) => {
                        errorMessage(err.message);
                        resolve(true);
                      });


                  })
                  .catch((err) => {
                    errorMessage(`Something wrong with ${item.name}`);
                    resolve(true);
                  });

              }));
          }

        });

        return Promise.all(countries);
      })
      .catch((error) => {
        errorMessage(error);
        return Promise.all(countries);
      });
  }
}

module.exports = CountryProxy;
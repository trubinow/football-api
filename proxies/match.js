class MatchProxy {
  constructor(mongoRepo, apiRepo) {
    this.mongoRepo = mongoRepo;
    this.apiRepo  = apiRepo;
  }

  /**
   * @returns {Promise<[Match]>}
   */
  getList(filter = {}, pagination = {page :1, perPage: 50}) {
    return new Promise((resolve, reject) => {
      this.mongoRepo.getList(filter, pagination)
        .then((models) => {
          resolve(models);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  /**
   * @returns {Promise<[Match]>}
   */
  getAssets(filter = {}, pagination = {page :1, perPage: 50}) {
    return new Promise((resolve, reject) => {
      this.mongoRepo.getAssets(filter, pagination)
        .then((models) => {
          resolve(models);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }


  /*
  getList(date) {
    return new Promise((resolve, reject) => {
      this.mongoRepo.getList(date)
        .then((modelFromMongo) => {

          if (modelFromMongo) {
            resolve(modelFromMongo);
          } else {
            this.apiRepo.getList(date)
              .then((modelFromApi) => {

                if (modelFromApi) {
                  this.mongoRepo.saveList(modelFromApi)
                    .then((model) => {
                      resolve(model);
                    })
                    .catch(() => {
                      resolve(modelFromApi);
                    })
                } else {
                  resolve(null);
                }
              })
              .catch((error) => {
                console.log(error);
                reject(error);
              });
          }
        })
        .catch((error) => {
          reject(error);
        })
    });
  }
  */

}

module.exports = MatchProxy;
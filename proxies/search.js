class SearchProxy {
  constructor(searchMongoRepo, searchTmdbRepo, searchConfig) {
    this.searchMongoRepo = searchMongoRepo;
    this.searchTmdbRepo  = searchTmdbRepo;
    this.searchConfig = searchConfig;
  }

  /**
   * @param id
   * @returns {Promise<search>}
   */
  get(keyword) {

    keyword = keyword.toLowerCase();

    return new Promise((resolve, reject) => {
      this.searchMongoRepo.get(keyword)
        .then((modelFromMongo) => {
          if (modelFromMongo) {
            resolve(modelFromMongo);
          } else {
            this.searchTmdbRepo.get(keyword)
              .then((modelFromTmdb) => {
                if (modelFromTmdb) {
                  this.searchMongoRepo.save(modelFromTmdb, this.searchConfig.ttl || null)
                    .then((model) => {
                      resolve(model);
                    })
                    .catch(() => {
                      resolve(modelFromTmdb);
                    });
                } else {
                  resolve(null);
                }
              })
              .catch((error) => {
                reject(error);
              });
          }
        })
        .catch((error) => {
          reject(error);
        });
    });
  }
}

module.exports = SearchProxy;
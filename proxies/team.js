const config = require('config');
const url = require('url');
const wikiUrl = config.get('apis.wikipediaorg.host');
const wikiList = config.get('apis.wikipediaorg.list');
const wikiMask = config.get('apis.wikipediaorg.mask');

const infoMessage  = require('debug')('team-proxy:info');
const errorMessage = require('debug')('team-proxy:error');
const cheerio = require('cheerio');

const wikiProvider = require('../providers/wikipedia');

const {download, deleteAll} = require('../other/download');
const decode = require('../other/decode');

class TeamProxy {
  constructor(mongoRepo, apiRepo) {
    this.mongoRepo = mongoRepo;
    this.apiRepo = apiRepo;
  }

  save(item) {
    return this.mongoRepo.save(item);
  }

  /**
   * @returns {Promise<[Country]>}
   */
  getList(filter = {}, pagination = {page :1, perPage: 50}, admin = false) {
    return new Promise((resolve, reject) => {
      this.mongoRepo.getList(filter, pagination, admin)
        .then((models) => {
          resolve(models);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  /**
   * @returns {Promise<[Country]>}
   */
  getCount(filter = {}) {
    return new Promise((resolve, reject) => {
      this.mongoRepo.getCount(filter)
        .then((count) => {
          resolve(count);
        })
        .catch((error) => {
          errorMessage(error);
          resolve(0);
        });
    });
  }

  getImages(filter = {}, pagination = {page :1, perPage: 50}, step = 1) {
    let teams = [];

    return this.mongoRepo.getList(filter, pagination)
      .then((models) => {
        infoMessage(`Wikipedia INIT: step ${step}`);

        models.forEach((item) => {

          if (item.name && item.image == '') {

            let url = '';

            teams.push(
              new Promise((resolve, reject) => {

                switch (step) {
                  case 1: url = wikiProvider.stepOne(item); break;
                  case 2: url = wikiProvider.stepTwo(item); break;
                  case 3: url = wikiProvider.stepThree(item); break;
                  case 4: url = wikiProvider.stepFour(item); break;
                  default: url = wikiProvider.stepOne(item);
                }

                this.apiRepo.api.getData(url)
                  .then((body) => {

                    if (step == 3 || step == 4) {
                      const $ = cheerio.load(body);
                      const qurl = $('.mw-search-result-heading a');

                      url = qurl.length > 0 && 'attribs' in qurl[0] ? `${wikiUrl}${qurl[0].attribs.href}` : null;

                      if (url) {
                        return this.apiRepo.api.getData(url)
                      } else {
                        return '';
                      }

                    } else {
                      return body;
                    };

                  })
                  .then((body) => {

                    if (body.indexOf('football') >= 0) {
                      const $ = cheerio.load(body);
                      const imgArr = $(wikiMask);
                      const src = imgArr.length > 0 && 'attribs' in imgArr[0] ? imgArr[0].attribs.src : null;

                      if (src && src.indexOf('left_arm') == -1) {
                        infoMessage(`Logo obtained for team ${item.name} -- ${src}`);
                        item.image = src;
                        item.sourcePage = url;
                        item.sourceMask = wikiMask;
                      } else {
                        item.step = step + 1;
                      }

                    } else {
                      item.step = step + 1;
                    }

                    this.mongoRepo.save(item)
                      .then(() => {
                        resolve(true);
                      })
                      .catch((err) => {
                        errorMessage(err.message);
                        resolve(true);
                      })

                  })
                  .catch(() => {
                    errorMessage(`Something wrong with ${item.name}`);
                    item.step = step + 1;

                    this.mongoRepo.save(item)
                      .then(() => {
                        resolve(true);
                      })
                      .catch((err) => {
                        errorMessage(err.message);
                        resolve(true);
                      })
                  });

              }));
          }

        });

        return Promise.all(teams);
      })
      .catch((error) => {
        errorMessage(error);
        return Promise.all(teams);
      });
  }

  getBase64Images(filter = {}, pagination = {page :1, perPage: 50}) {

    return this.mongoRepo.getList(filter, pagination)
      .then((res) => {

        infoMessage('Teams list :' + res.length);

        return res
          .reduce((res, item, index) => {
            if (item.id && item.image && item.image.indexOf('wikipedia') != -1) {
              res.push({id: item.id, image: item.image})
            }

            return res;

          }, [])
      })
      .then((res) => {

        infoMessage('Teams list(after filter) :' + res.length);

        return Promise.all(
          res
            .map((item) => {

              let imgUrl = 'http:' + item.image.replace(/[0-9]{1,3}px-/, 64 + 'px-');
              let fileName = url.parse(imgUrl).pathname.split('/').pop();

              fileName = `./temp/${fileName}`;

              return download(imgUrl, item.id, fileName);
            })
        );
      })
      .then((res) => {

        infoMessage('Teams list(After download) :' + res.length);

        return Promise.all(
          res
            .filter((item) => {
              return item !== false;
            })
            .map((item) => {
              return decode(item);
            })
        )
      })
      .then((res) => {

        infoMessage('Teams list(After decode) :' + res.length);

        return Promise.all(
          res
            .filter((item) => {
              return item !== false;
            })
            .map((item) => {
              return this.mongoRepo.save(item);
            })
        )
      })
      .then((res) => {
        infoMessage(`Done: ${res.length}`);

        deleteAll(`./temp`);
      })
      .catch((error) => {
        errorMessage(error);
      });

  }

  getAnyImage(url) {

    return new Promise((resolve, reject) => {
      this.apiRepo.api.getData(url)
        .then(body => {
          const $ = cheerio.load(body);
          const qurl = $(wikiMask);

          resolve(qurl.length > 0 && 'attribs' in qurl[0]
            ? {
                image: qurl[0].attribs.src,
                sourcePage: url,
                mask: wikiMask
              }
            : false);
        })
        .catch((err) => {
          resolve(false);
        })
    })
  }

  getVariants(query) {
    return new Promise((resolve, reject) => {

      this.apiRepo.api.getData(wikiProvider.stepThree({name: query}))
        .then((body) => {
          const $ = cheerio.load(body);
          const qurl = $('.mw-search-result-heading a').toArray();


            Promise.all(
              qurl
                .reduce((res, val, index) => {
                  if (index < 10 ) {
                    res.push({
                      title: val.attribs.title || '',
                      href: wikiUrl + val.attribs.href || ''
                    })
                  }

                  return res;
                }, [])
                .reduce((res, val) => {

                  if (val.href) {
                    res.push(this.getAnyImage(val.href));
                  }

                  return res;
                }, [])
            )
              .then(data => {
                resolve(
                  data.filter((item) => {
                    return item !== false;
                  })
                );
              })
              .catch(err => {
                resolve(err);
              })


        })
        .catch((err) => {
          resolve(err);
        })
    });
  }

  /**
   * @returns {Promise<[Team]>}
   */
  getAssets(filter = {}, pagination = {page :1, perPage: 50}) {
    return new Promise((resolve, reject) => {
      this.mongoRepo.getAssets(filter, pagination)
        .then((models) => {
          resolve(models);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }
}

module.exports = TeamProxy;
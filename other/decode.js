const fs = require('fs');
const infoMsg = require('debug')('decode:info');
const errorMsg = require('debug')('decode:error');

module.exports = (item) => {

  return new Promise((resolve) => {

    if (!item.teamId || !item.filename) {
      errorMsg(`Wrong data`);
      resolve(false);
    } else {
      fs.readFile(item.filename, 'base64', function (err, imageBase) {

        if (err) {
          errorMsg(err);
          resolve(false);
        } else {
          infoMsg(`${item.teamId} is decoded`);
          resolve({id: item.teamId, imageBase: item.code + imageBase});
        }
      });
    };
  });
};
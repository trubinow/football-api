const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

let User = require('../mongo/models/Account');

passport.use(new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password',
    session: false
  },
  function(username, password, done) {
    User.findOne({username: username}, function(err, user) {
      if (err) {
        return done(err);
      }

      if (!user) {
        return done(null, false, {
          message: 'Incorrect username/email'
        })
      }

      if (!user.validPassword(password)){
        return done(null, false, {
          message: 'Incorrect password'
        });
      }

      if (!user.isActive()){
        return done(null, false, {
          message: 'User is disabled'
        });
      }

      return done(null, user);
    });
  }
));
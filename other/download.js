const infoMsg = require('debug')('download:info');
const errorMsg = require('debug')('download:error');

const fs = require('fs');
const path = require('path');
const request = require('request');
const Bootleneck = require('bottleneck');

const limiter = new Bootleneck({
  reservoir: 100, // initial value
  reservoirRefreshAmount: 100,
  reservoirRefreshInterval: 60 * 1000 // must be divisible by 250
});

function download (url, teamId, filename, callback) {
  request.head(url, (err, res, body) => {

    if (err) {
      errorMsg(err);
      callback(err, teamId, filename);
    } else {

      infoMsg(`Download function: ${teamId}`);

      request(url)
        .on('error', (err) => {
          errorMsg(err);
          callback(err, teamId, filename)
        })
        .on('response', function(res) {
          console.log(res.statusCode) // 200
          console.log(res.headers['content-type']) // 'image/png'
          callback(null, teamId, filename, "data:" + res.headers["content-type"] + ";base64,")
        })
        .on('close', () => {
          infoMsg(filename);
          //callback(null, teamId, filename, "data:" + res.headers["content-type"] + ";base64,")
        })
        .pipe(fs.createWriteStream(filename))
    };

  })
}

function deleteFile(filename) {
  return new Promise((resolve) => {
    fs.unlink(filename, function(err){
      if(err) {
        errorMsg(err);
        resolve(false);
      } else {
        infoMsg('file deleted successfully');
        resolve(true);
      }
    });
  });
}

function deleteAll(directory) {

  fs.readdir(directory, (err, files) => {
    if (err) throw err;

    for (const file of files) {
      fs.unlink(path.join(directory, file), err => {
        if (err) throw err;
      });
    }
  });
}

function downloadPromise(url, teamId, filename) {
  return new Promise((resolve) => {
    let err = false;
    limiter.submit(download, url, teamId, filename, (err, teamId, filename, code) => {

      if (err) {
        errorMsg(err);
        resolve(false);
      } else {
        infoMsg(`Downloaded: ${filename}`);
        resolve({teamId: teamId, filename: filename, code: code});
      };
    });
  })
}

module.exports = {
  download: downloadPromise,
  deleteAll: deleteAll
}
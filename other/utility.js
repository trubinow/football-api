const config = require('config');
const infoMsg = require('debug')('utility:info');
const errorMsg = require('debug')('utility:error');

const redis = require('redis');

// mongo
require('../mongo/db')();

const LimiterClass = require('../services/limiter');
const ApiClass = require('../services/api');

const GoalComParserClass = require('../services/parser');
const AllSoccerParserClass = require('../services/parser-all-soccer');

const ListPopulateService = require('../services/list-populate');

const redisConfig = config.get('redis');
const limiterConfig = config.get('limiter');
const apiConfig = config.get('apis.apifootballcom');

const redisClient = redis.createClient(redisConfig.port, redisConfig.host);

const limiter = new LimiterClass(limiterConfig.reqPerTenSeconds, limiterConfig.queueId, redisConfig);
const api = new ApiClass(limiter, apiConfig);

const matches = apiConfig.methods.matches && apiConfig.methods.matches == 'parser' ? new AllSoccerParserClass(limiter) : api;

// repositories
const CountryMongoRepository = require('../repository/country/mongo-repository');
const CountryApiRepository = require('../repository/country/api-repository');
const CountryProxy = require('../proxies/country');

const LeagueMongoRepository = require('../repository/league/mongo-repository');
const LeagueApiRepository = require('../repository/league/api-repository');
const LeagueProxy = require('../proxies/league');

const TeamMongoRepository = require('../repository/team/mongo-repository');
const TeamApiRepository  = require('../repository/team/api-repository');
const TeamProxy = require('../proxies/team');

const MatchMongoRepository = require('../repository/match/mongo-repository');
const MatchApiRepository  = require('../repository/match/api-repository');
const MatchProxy = require('../proxies/match');


const countryMongoRepository = new CountryMongoRepository();
const countryApiRepository = new CountryApiRepository(api);

const leagueMongoRepository = new LeagueMongoRepository();
const leagueApiRepository = new LeagueApiRepository(api);

const teamMongoRepository = new TeamMongoRepository();
const teamApiRepository      = new TeamApiRepository(api);

const matchMongoRepository = new MatchMongoRepository();
const matchApiRepository      = new MatchApiRepository(matches);

const matchesService = new ListPopulateService(matchMongoRepository, matchApiRepository, redisClient);

let mw = function (req, res, next) {

  let countryProxy = new CountryProxy(countryMongoRepository, countryApiRepository);
  req.countryProxy = countryProxy;
  req.countriesPopulateService = new ListPopulateService(countryMongoRepository, countryApiRepository, redisClient, countryProxy);

  let leagueProxy = new LeagueProxy(leagueMongoRepository, leagueApiRepository);
  req.leagueProxy = leagueProxy;
  req.leaguesPopulateService = new ListPopulateService(leagueMongoRepository, leagueApiRepository, redisClient, leagueProxy);

  let teamProxy = new TeamProxy(teamMongoRepository, teamApiRepository);
  req.teamProxy = teamProxy;
  req.teamsPopulateService = new ListPopulateService(teamMongoRepository, teamApiRepository, redisClient, teamProxy);

  req.matchProxy = new MatchProxy(matchMongoRepository, matchApiRepository);
  req.matchesPopulateService = matchesService;

  next();
};

let matchesUpdater =  function (interval = 24 * 60 * 60 * 1000) {

  infoMsg(`Interval: ${interval}`);

  setInterval(() => {
    matchesService.run()
      .catch((error) => {
        errorMsg(error.message);
      });
  }, interval);
};

module.exports = {
  mw: mw,
  matchesUpdater: matchesUpdater
};
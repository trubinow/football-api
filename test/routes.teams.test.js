//During the test the env variable is set to test
process.env.NODE_ENV = 'development';

let chai = require('chai');
let chaiHttp = require('chai-http');

chai.should();
chai.use(chaiHttp);

describe('Routes: Teams', () => {

  beforeEach((done) => {
    done();
  });

  describe('/GET teams', () => {
    it('it should GET matches', (done) => {
      chai.request('http://localhost:8009')
        .get('/api/leagues/63/teams')
        .end((err, res) => {

          res.should.have.status(200);
          res.body.data.items.should.be.a('array');
          res.body.data.items.length.should.not.be.eql(0);
          done();
        });
    });
  });

  describe('/GET admin teams', () => {
    it('it should GET matches', (done) => {
      chai.request('http://localhost:8009')
        .get('/api/admin/teams')
        .end((err, res) => {

          res.should.have.status(200);
          res.body.data.items.should.be.a('array');
          res.body.data.items.length.should.not.be.eql(0);
          done();
        });
    });
  });

  describe('/GET admin team', () => {
    it('it should GET matches', (done) => {
      chai.request('http://localhost:8009')
        .get('/api/admin/teams/Nottingham Forest')
        .end((err, res) => {

          res.should.have.status(200);
          res.body.data.items.should.be.a('array');
          res.body.data.items.length.should.be.eql(1);
          done();
        });
    });
  });

});
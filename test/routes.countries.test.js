//During the test the env variable is set to test
process.env.NODE_ENV = 'development';

let chai = require('chai');
let chaiHttp = require('chai-http');

chai.should();
chai.use(chaiHttp);

describe('Routes: Countries', () => {

  beforeEach((done) => {
    done();
  });

  describe('/GET countries', () => {
    it('it should GET countries', (done) => {
      chai.request('http://localhost:8009')
        .get('/api/countries')
        .end((err, res) => {

          res.should.have.status(200);
          res.body.data.items.should.be.a('array');
          res.body.data.items.length.should.not.be.eql(0);
          done();
        });
    });
  });

  describe('/GET admin countries', () => {
    it('it should GET countries', (done) => {
      chai.request('http://localhost:8009')
        .get('/api/admin/countries')
        .end((err, res) => {

          res.should.have.status(200);
          res.body.data.items.should.be.a('array');
          res.body.data.items.length.should.not.be.eql(0);
          done();
        });
    });
  });

  describe('/GET admin country', () => {
    it('it should GET country', (done) => {
      chai.request('http://localhost:8009')
        .get('/api/admin/countries/Champions League')
        .end((err, res) => {

          res.should.have.status(200);
          res.body.data.items.should.be.a('array');
          res.body.data.items.length.should.be.eql(1);
          done();
        });
    });
  });

});
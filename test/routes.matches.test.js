//During the test the env variable is set to test
process.env.NODE_ENV = 'development';

let chai = require('chai');
let chaiHttp = require('chai-http');

chai.should();
chai.use(chaiHttp);

describe('Routes: Matches', () => {
  beforeEach((done) => {
    done();
  });

  describe('/GET matches', () => {
    it('it should GET matches', (done) => {
      chai.request('http://localhost:8009')
        .get('/api/matches')
        .end((err, res) => {

          res.should.have.status(200);
          res.body.data.items.should.be.a('array');
          res.body.data.items.length.should.not.be.eql(0);
          done();
        });
    });
  });

});
process.env.NODE_ENV = 'development';
const chai = require('chai');
const dateFormat = require('dateformat');

const config           = require('config');
const limiterConfig    = config.get('limiter');
const reqPerTenSeconds = limiterConfig.reqPerTenSeconds || 38;
const queueId          = limiterConfig.queueId || null;
const redisConfig      = config.get('redis');
//const apiConfig       = config.get('apis.apifootballcom');

const LimiterClass = require('../services/limiter');
const ApiClass = require('../services/api');
const AllSoccerParserClass = require('../services/parser-all-soccer');
const ListPopulateService = require('../services/list-populate');

const limiter = new LimiterClass(reqPerTenSeconds, queueId, redisConfig);
//const api = new ApiClass(limiter, apiConfig);
const parser = new AllSoccerParserClass(limiter);

// mongo
require('../mongo/db')();
const redis = require('redis');
const redisClient = redis.createClient(redisConfig.port, redisConfig.host);

const MatchMongoRepository = require('../repository/match/mongo-repository');
const MatchApiRepository  = require('../repository/match/api-repository');
const MatchProxy = require('../proxies/match');

const ModelMongo = require('../mongo/models/Match');

const matchMongoRepository = new MatchMongoRepository();
const matchApiRepository      = new MatchApiRepository(parser);//api

const matchesService = new ListPopulateService(matchMongoRepository, matchApiRepository, redisClient);

chai.should();

let now = new Date().getTime();
let today = dateFormat(now, 'yyyy-mm-dd');

describe('Matches service test', () => {
  before(function (done) {

    ModelMongo.remove({date: today})
      .then(() => {
        done();
      });
  });

  it('populate matches', (done) => {
    matchesService.run()
      .then(() => {

        setTimeout(function() {

          ModelMongo.find({date: today})
            .exec()
            .then((res) => {
              res.should.be.a('array');

              res.should.lengthOf.above(0);

              res[0].should.be.a('object');

              res[0].should.have.property('id').not.eql(null);

              res[0].should.have.property('countryName');
              res[0].should.have.property('leagueName');

              res[0].should.have.property('awayTeamName');
              res[0].should.have.property('homeTeamName');

              res[0].should.have.property('date');
              res[0].should.have.property('time');

              done();
            })
            .catch((error) => {
              done(error);
            })

        }, 3000);

      })
      .catch((error) => {
        done(error);
      });

  }).timeout(60000);

});
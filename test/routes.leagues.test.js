//During the test the env variable is set to test
process.env.NODE_ENV = 'development';

let chai = require('chai');
let chaiHttp = require('chai-http');

chai.should();
chai.use(chaiHttp);

describe('Routes: Leagues', () => {

  beforeEach((done) => {
    done();
  });

  describe('/GET leagues', () => {
    it('it should GET leagues', (done) => {
      chai.request('http://localhost:8009')
        .get('/api/countries/169/leagues')
        .end((err, res) => {

          res.should.have.status(200);
          res.body.data.items.should.be.a('array');
          res.body.data.items.length.should.not.be.eql(0);
          done();
        });
    });
  });

  describe('/GET admin leagues', () => {
    it('it should GET leagues', (done) => {
      chai.request('http://localhost:8009')
        .get('/api/admin/leagues')
        .end((err, res) => {

          res.should.have.status(200);
          res.body.data.items.should.be.a('array');
          res.body.data.items.length.should.not.be.eql(0);
          done();
        });
    });
  });

  describe('/GET admin leagues', () => {
    it('it should GET league', (done) => {
      chai.request('http://localhost:8009')
        .get('/api/admin/leagues/796')
        .end((err, res) => {

          res.should.have.status(200);
          res.body.data.items.should.be.a('array');
          res.body.data.items.length.should.be.eql(1);
          done();
        });
    });
  });

});
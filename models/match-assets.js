class MatchAssets {
  constructor({
                homeTeamId = '',
                awayTeamId = '',
                homeImageBase = '',
                awayImageBase = ''
              } = {}) {

      this.homeTeamId = homeTeamId;
      this.awayTeamId = awayTeamId;

      this.homeImageBase = homeImageBase;
      this.awayImageBase = awayImageBase;
  }
}

module.exports = MatchAssets;
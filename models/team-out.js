class TeamOut {
  constructor({
                id = null,
                name = "",
                countryName = "",
                countryId = null,
                image = '',

  } = {}) {
    this.id = id;
    this.name = name;
    this.countryId = countryId;
    this.countryName = countryName;
    this.image = image;
  }
}

module.exports = TeamOut;
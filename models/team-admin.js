class TeamOut {
  constructor({
                id = null,
                name = "",
                countryName = "",
                countryId = null,
                image = '',
                sourcePage = '',
                sourceMask = ''

  } = {}) {
    this.id = id;
    this.name = name;
    this.countryId = countryId;
    this.countryName = countryName;
    this.image = image;
    this.sourcePage = sourcePage;
    this.sourceMask = sourceMask;
  }
}

module.exports = TeamOut;
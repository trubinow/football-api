class Match {
  constructor({
                id = null,

                countryId = null,
                countryName = "",

                leagueId = null,
                leagueName = "",

                date = null,
                time = null,
                status = '',
                image = '',

                homeTeamId = '',
                awayTeamId = '',

                homeTeamName = '',
                homeTeamScore = '',

                awayTeamName = '',
                awayTeamScore = '',

                awayImage = '',
                homeImage = '',

                goalScorer = [],
                cards = [],
                lineup = [],
                statistics = [],
                live = false,
                matchTimeStamp = 0,

              } = {}) {

      this.id = id;

      this.countryId = countryId;
      this.countryName = countryName;

      this.leagueId = leagueId;
      this.leagueName = leagueName;

      this.date = date;
      this.time = time;
      this.status = status;
      this.image = image;

      this.homeTeamId = homeTeamId;
      this.awayTeamId = awayTeamId;

      this.homeTeamName = homeTeamName;
      this.homeTeamScore = homeTeamScore;

      this.homeImage = homeImage;
      this.awayImage = awayImage;

      this.awayTeamName = awayTeamName;
      this.awayTeamScore = awayTeamScore;

      this.goalScorer = goalScorer;
      this.cards = cards;
      this.lineup = lineup;
      this.statistics = statistics;
      this.live = live;
      this.matchTimeStamp = matchTimeStamp;
  }
}

module.exports = Match;
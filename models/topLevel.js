class TopLevel {
  constructor({ data = null,
                appVersion = '1.0',
                method = '',
                page = 1,
                itemsPerPage = null,
                params = null,
                error = null,
                total = null
              } = {}) {

    this.appVersion = appVersion;

    let count = Array.isArray(data) ? data.length: 0;

    if (data)
      this.data = { currentItemCount: count, pageIndex: page, items: data};

    if (data && itemsPerPage) {
      this.data.itemsPerPage = itemsPerPage;

      if (count == 0 || count < itemsPerPage) {
        this.data.nextPageIndex = false;
      } else {
        this.data.nextPageIndex = page + 1;
      }
    }

    if (total)
      this.data.total = total;

    if (method)
      this.method = method;

    if (params)
      this.params = params;

    if (error)
      this.error = error;
  }
}

module.exports = TopLevel;
class Country {
  constructor({
                id = null,
                name = "",
                image = ""

  } = {}) {
    this.id = id;
    this.name = name;
    this.image = image;
  }
}

module.exports = Country;
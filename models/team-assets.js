class TeamAsset {
  constructor({
                id = null,
                imageBase = '',

  } = {}) {
    this.id = id;
    this.imageBase = imageBase;
  }
}

module.exports = TeamAsset;
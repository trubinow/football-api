class Team {
  constructor({
                id = null,
                name = "",
                leagueId = null,
                countryName = "",
                countryId = null,
                image = '',

  } = {}) {
    this.id = id;
    this.name = name;
    this.leagueId = leagueId;
    this.countryId = countryId;
    this.countryName = countryName;
    this.image = image;
  }
}

module.exports = Team;
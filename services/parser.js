const request = require('request-promise');

const cheerio = require('cheerio');
const dateFormat = require('dateformat');

const infoMessage = require('debug')('Parser:info');
const errorMessage = require('debug')('Parser:error');

class Parser {
  constructor(limiter) {
    this.host = 'https://www.goal.com/en/live-scores';//config.host;
    this.limiter = limiter;
  }

  /**
   * @param url
   * @returns {Promise<Object>}
   */
  getData(url, data) {
    infoMessage(url);

    const req = {
      url: url,
      json: true
    };

    return new Promise((resolve, reject) => {
      this.limiter.apiCall(() => {
        return request(req)
          .then((result) => {

            if (data && Array.isArray(result)) {
              resolve(result.map((item) => Object.assign(item, data)));
            } else {
              resolve(result);
            }

          })
          .catch((error) => {
            reject(error);
          });
      });
    });
  }

  /**
   * @param page
   * @returns {Promise<Object>}
   */
  getList() {
    return new Promise((resolve, reject) => {

      let results = [];
      let now = new Date();
      let today = dateFormat(now, 'yyyy-mm-dd');

      this.getData(this.host)
        .then((body) => {
          let $ = cheerio.load(body);
          let leagues = $('div.competition-matches').toArray();

          leagues.forEach(league => {
            let leagueName = $('.competition-name', league).text().trim();
            let matches = $('div.match-main-data', league).toArray();

            matches.forEach(match => {
              let res = {};
              let teamAway = $('.team-away', match);
              let teamHome = $('.team-home', match);

              res.source = 'parser';
              res.id = '1';
              res.leagueName = leagueName;
              res.leagueId = '';

              res.date = today;
              res.time = $('.match-status time', match).text().trim();

              res.status = $('.match-status span', match).text().trim();

              res.live = $('.match-status time', match).attr('data-utc') - now > 0
                            && Math.abs($('.match-status time', match).attr('data-utc') - now) < 6000000 ? true : false;

              res.matchTimeStamp = $('.match-status time', match).attr('data-utc');

              res.awayTeamName = $('.team-name', teamAway).text() || '-';
              res.homeTeamName = $('.team-name', teamHome).text() || '-';

              res.awayTeamScore = $('.goals', teamAway).text() || '0';
              res.homeTeamScore = $('.goals', teamHome).text() || '0';

              results.push(res);
            });

          });

          infoMessage(`Parsed ${results.length} items from GOALS.com`);
          resolve(results);
        })
        .catch((err) => {
          errorMessage(err.message);
          resolve(results);
        });
    });

  }
}

module.exports = Parser;

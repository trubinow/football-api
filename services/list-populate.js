const infoMessage  = require('debug')('list-populate-service:info');
const resInfoMessage  = require('debug')('list-populate-service:log');
const errorMessage = require('debug')('list-populate-service:error');

/**
 * Used to fill the list of movies top rated and shows.
 * apiRepo must contain getTop method
 * mongoRepo must contain saveTop method
 * redisClient must contain get method
 */
class ListPopulateService {
  constructor(mongoRepo, apiRepo, redisClient, proxy) {
    this.mongoRepo           = mongoRepo;
    this.apiRepo            = apiRepo;
    this.redisClient         = redisClient;
    this.redisKey            = 'is_list_populating';
    this.redisExperationTime = 1 * 60 * 60; // 1h * 60m * 60s = 86400s
    this.proxy = proxy;
  }

  isProcessing() {
    return new Promise((resolve, reject) => {
      this._getRedisValue(this.redisKey)
        .then((result) => {
          if (result === '1' && result !== null) {
            resolve(true);
          } else {
            resolve(false);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  run(parser) {

    return this.isProcessing()
      .then((isProcessing) => {
        if (!isProcessing) {
          return parser != undefined ? this._populateVarious(parser) : this._populate();
        } else {
          infoMessage('populate top already in progress.');
        }
      });
  }

  runImagesImport(filter = {}, pagination = {page: 1, perPage: 5000}, step = 1) {

    return this.isProcessing()
      .then((isProcessing) => {
        if (!isProcessing) {
          this.proxy.getImages(filter, pagination, step)
            .then((res) => {
              infoMessage(`Import images completed: ${res.length}`);
              this._setIsDone();
            })
            .catch((err) => {
              errorMessage(`Import images completed: ${err.message}`);
              this._setIsDone();
            });
        } else {
          infoMessage('populate top already in progress.');
        }
      });
  }

  runImagesDecode(filter = {}, pagination = {page: 1, perPage: 5000}) {

    return this.isProcessing()
      .then((isProcessing) => {
        if (!isProcessing) {
          this.proxy.getBase64Images(filter, pagination)
            .then((res) => {
              infoMessage(`Base64 decoding completed: ${res.length}`);
              this._setIsDone();
            })
            .catch((err) => {
              errorMessage(`Base64 decoding completed: ${err.message}`);
              this._setIsDone();
            });
        } else {
          infoMessage('populate top already in progress.');
        }
      });
  }

  _getRedisValue(key) {
    return new Promise((resolve, reject) => {
      this.redisClient.get(key, (error, result) => {
        if (error) {
          reject(error);
        } else {
          resolve(result);
        }
      }
      );
    });
  }

  _setInProgress() {
    this.redisClient.set(this.redisKey, '1', 'EX', this.redisExperationTime);
  }

  _setIsDone() {
    this.redisClient.set(this.redisKey, '0', 'EX', this.redisExperationTime);
  }

  _populate() {
    this._setInProgress();
    return this.apiRepo.getList()
      .then((models) => {

        if (models && models.length > 0) {

          resInfoMessage(`Obtained: ${models.length} items.`);

          //return this.mongoRepo._clearAll()
          //  .then(() => {
              this.mongoRepo.saveList(models)
                .then(() => {
                  resInfoMessage('Saved.');
                  this._setIsDone();
                })
                .catch(() => {
                  this._setIsDone();
                });
          //  });

        } else {
          throw new Error('could not get top list from API[0]');
        }
      })
      .catch((error) => {
        this._setIsDone();
        errorMessage(error.message);
        //throw new Error('could not get top list from API');
      });
  }

  _populateVarious(parser) {
    this._setInProgress();

    return this.apiRepo.getList()
      .then((models) => {
        if (models && models.length > 0) {
          return models;
        } else {
          return parser ? parser.getList(): [];
        }
      })
      .then((models) => {

        if (models && models.length > 0) {

          return this.mongoRepo._clearAll()
            .then(() => {
              this.mongoRepo.saveList(models)
                .then(() => {
                  this._setIsDone();
                  resInfoMessage('Saved.');
                })
                .catch((error) => {
                  this._setIsDone();
                  errorMessage(error.message);
                  throw new Error('could not get top list from Parser');
                });
            });
        } else {
          this._setIsDone();
          throw new Error('could not get top list from Parser[0]');
        }
      })
      .catch((error) => {
        this._setIsDone();
        errorMessage(error.message);
        throw new Error('could not get top list from API');
      });

  }
}

module.exports = ListPopulateService;
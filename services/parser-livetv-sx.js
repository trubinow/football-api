const request = require('request-promise');
const moment = require('moment');
require('moment-timezone');
const cheerio = require('cheerio');
const dateFormat = require('dateformat');

const infoMessage = require('debug')('Parser:info');
const errorMessage = require('debug')('Parser:error');

class Parser {
  constructor(limiter) {
    this.host = 'http://livetv.sx/enx/allupcomingsports/1/';
    this.limiter = limiter;
  }

  /**
   * @param url
   * @returns {Promise<Object>}
   */
  getData(url, data) {
    infoMessage(url);

    const req = {
      url: url,
      json: true
    };

    return new Promise((resolve, reject) => {
      //this.limiter.apiCall(() => {
        return request(req)
          .then((result) => {

            if (data && Array.isArray(result)) {
              resolve(result.map((item) => Object.assign(item, data)));
            } else {
              resolve(result);
            }

          })
          .catch((error) => {
            reject(error);
          });
      });
    //});
  }

  /**
   * @param page
   * @returns {Promise<Object>}
   */
  getList() {
    return new Promise((resolve, reject) => {

      let results = [];
      let timezoneoffset = (new Date()).getTimezoneOffset()*60*1000;

      infoMessage(`Timezoneoffset(ms): ${timezoneoffset}`);

      let now = new Date().getTime() + timezoneoffset;
      let today = dateFormat(now, 'yyyy-mm-dd');

      this.getData(this.host.replace('%CURRENT-DATE%', dateFormat(now, 'dd.mm.yyyy')))
        .then((body) => {

          let $ = cheerio.load(body);



          let res = {};

          res.awayTeamName = 'Arsenal';
          res.homeTeamName = 'Borrussia';
          //....


          results.push(res);


          infoMessage(`Parsed ${results.length} items from livetv.sx`);
          resolve(results);
        })
        .catch((err) => {
          errorMessage(err.message);
          resolve(results);
        });
    });

  }
}

module.exports = Parser;
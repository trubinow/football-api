const request = require('request-promise');

const infoMessage = require('debug')('api:info');

class Api {
  constructor(limiter, config) {
    this.apikey  = config.apikey;
    this.host    = config.host;
    this.methods = config.methods;
    this.limiter = limiter;
  }

  /**
   * @param url
   * @returns {Promise<Object>}
   */
  getData(url, data) {
    infoMessage(url);

    const req = {
      url : url,
      json: true
    };

    return new Promise((resolve, reject) => {
      this.limiter.apiCall(() => {
        return request(req)
          .then((result) => {

            if (data && Array.isArray(result)) {
              resolve(result.map((item) => Object.assign(item, data)));
            } else {
              resolve(result);
            }

          })
          .catch((error) => {
            reject(error);
          });
      });
    });
  }

  /**
   * @param page
   * @returns {Promise<Object>}
   */
  getList(method, paramStr = '', page = 1) {

    let endpoint = `${this.host}`
      .replace('%action%', this.methods[method])
      .replace('%apikey%', this.apikey)
      .replace('%page%', page);

    endpoint += paramStr;

    return this.getData(endpoint);
  }

  /**
   * @param page
   * @returns {Promise<Object>}
   */
  getCountriesList(page = 1) {

    let endpoint = `${this.host}`
      .replace('%action%', this.methods.countries)
      .replace('%apikey%', this.apikey)
      .replace('%page%', page);

    return this.getData(endpoint);
  }

  /**
   * @param page
   * @returns {Promise<Object>}
   */
  getTeamsList(method, paramStr = '', data = null, page = 1) {

    let endpoint = `${this.host}`
      .replace('%action%', this.methods[method])
      .replace('%apikey%', this.apikey)
      .replace('%page%', page);

    endpoint += paramStr;

    return this.getData(endpoint, data);
  }
}

module.exports = Api;

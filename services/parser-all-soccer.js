const request = require('request-promise');
const moment = require('moment');
require('moment-timezone');
const cheerio = require('cheerio');
const dateFormat = require('dateformat');

const infoMessage = require('debug')('Parser:info');
const errorMessage = require('debug')('Parser:error');

class Parser {
  constructor(limiter) {
    this.host = 'https://all.soccer/en?live=0&date=%CURRENT-DATE%&show_my_turnir=0&my_games=0&sort=1&odds=0';//config.host;
    this.limiter = limiter;
  }

  /**
   * @param url
   * @returns {Promise<Object>}
   */
  getData(url, data) {
    infoMessage(url);

    const req = {
      url: url,
      json: true
    };

    return new Promise((resolve, reject) => {
      this.limiter.apiCall(() => {
        return request(req)
          .then((result) => {

            if (data && Array.isArray(result)) {
              resolve(result.map((item) => Object.assign(item, data)));
            } else {
              resolve(result);
            }

          })
          .catch((error) => {
            reject(error);
          });
      });
    });
  }

  /**
   * @param page
   * @returns {Promise<Object>}
   */
  getList() {
    return new Promise((resolve, reject) => {

      let results = [];
      let timezoneoffset = (new Date()).getTimezoneOffset()*60*1000;

      infoMessage(`Timezoneoffset(ms): ${timezoneoffset}`);

      let now = new Date().getTime() + timezoneoffset;
      let today = dateFormat(now, 'yyyy-mm-dd');

      this.getData(this.host.replace('%CURRENT-DATE%', dateFormat(now, 'dd.mm.yyyy')))
        .then((body) => {

          let $ = cheerio.load(body);

          let timezoneArr = $('.timezone-select select option[selected="selected"]').toArray();

          let timezone = '';

          if (Array.isArray(timezoneArr) && timezoneArr.length == 1) {
            timezone = $(timezoneArr).attr('value');
          }

          let leaguesAndMatches = $('.m-table tr').toArray();

          let leagueName = '';

          leaguesAndMatches.forEach(item => {

            if ($(item).attr('class').indexOf('turnir-name') >= 0) {
              leagueName = $('h2', item).text();
            }

            if ($(item).attr('class').indexOf('match-row') >= 0) {

              let res = {};

              let teamAway = $('.visit-team', item);
              let teamHome = $('.home-team', item);

              res.source = 'parser';
              res.id = '1';

              res.leagueName = leagueName;

              let leagArr = leagueName.split(':');

              if (Array.isArray(leagArr) && leagArr.length > 1 ) {
                res.leagueName = leagArr.slice(1).join(':');
                res.countryName = leagArr[0].trim();
                res.countryName = res.countryName.charAt(0).toUpperCase() + res.countryName.slice(1);
              }

              res.date = today;
              res.time = $('.match-time', item).text().trim();

              res.status = $('.match-status .status', item).text().trim();

              if (res.status == 'Finished') {
                res.status = 'FT';
              }

              if (res.time != '-') {
                let datetime = moment.tz(`${res.date} ${res.time}`, timezone);
                datetime = datetime.tz('Africa/Accra');//GMT+0

                res.live = now - datetime < 6000000 && now - datetime > 0 ? true : false;
                res.status = now - datetime > 6000000 ? 'FT' : '';

                res.date = dateFormat(datetime, 'yyyy-mm-dd');
                res.time = dateFormat(datetime, 'HH:MM');
              }

              res.awayTeamName = $('.team-title a', teamAway).text().trim() || '-';
              res.homeTeamName = $('.team-title a', teamHome).text().trim() || '-';

              let score = $('.score a', item).text().split('-');

              res.awayTeamScore = score[1].trim() || '0';
              res.homeTeamScore = score[0].trim() || '0';

              if (res.time != '-' && res.time != '') {
                results.push(res);
              }
            }

          });

          infoMessage(`Parsed ${results.length} items from all.soccer`);
          resolve(results);
        })
        .catch((err) => {
          errorMessage(err.message);
          resolve(results);
        });
    });

  }
}

module.exports = Parser;
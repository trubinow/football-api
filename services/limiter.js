const Bottleneck = require('bottleneck');

class Limiter {

  constructor(reqCount, queueId, redisConfig) {
    const maxConcurrent = null; // how many requests can be running at the same time. 0 - unlimited
    const highWater     = null; //How long can the queue get? When the queue length exceeds that value, the selected strategy is executed to shed the load.
    const tenSeconds    = 10 * 1000; // 10 * 1000ms - 10s
    const minTime       = tenSeconds / reqCount; // how long to wait after launching a request before launching another one.


    if (queueId) {
      if (Object.keys(redisConfig).length > 0 && redisConfig.host && redisConfig.port) {
        const id            = queueId; // should be unique for every limiter in the same Redis db
        const datastore     = 'redis';
        const clientOptions = {
          host: redisConfig.host,
          port: redisConfig.port
        };

        this.limiter = new Bottleneck({maxConcurrent, highWater, minTime, id, datastore, clientOptions});
      } else {
        throw new Error('redis config not define.');
      }
    } else {
      throw new Error('queueId not define.');
    }
  }

  /**
   *
   * @param promise
   * @returns {*}
   */
  apiCall(promise) {
    return this.limiter.schedule(promise);
  }
}

module.exports = Limiter;

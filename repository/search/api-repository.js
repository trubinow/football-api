const errorMessage = require('debug')('api-search-repo:error');
const Search = require('../../models/Search');

class SearchTMDbRepository {
  /**
   * @param {TMDbApi} tmdbapi
   * @param {TMDbConfigurationService} tmdbConfigurationService
   */
  constructor(tmdbapi, tmdbConfigurationService) {
    this.tmdbapi                  = tmdbapi;
    this.tmdbConfigurationService = tmdbConfigurationService;
  }

  get(keyword) {
    return new Promise((resolve, reject) => {

      Promise.all([this.tmdbapi.getSearchMovies(keyword), this.tmdbapi.getSearchPersons(keyword)])
        .then(res => {
          let movies = res[0].results || [];
          let persons = res[1].results || [];

          resolve({keyword: keyword, movies: movies.slice(0,10), persons: persons.slice(0,10)});
        })
        .catch(err => {
          reject(err);
        });
    });

  }
}

module.exports = SearchTMDbRepository;
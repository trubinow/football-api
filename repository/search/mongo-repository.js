const infoMessage  = require('debug')('search-mongo:info');
const errorMessage = require('debug')('search-mongo:error');

// models
const Search = require('../../models/Search');

// mongo models
const SearchMongo     = require('../../mongo/models/Search');

class SearchMongoRepository {

  /**
   * @param model<Search>
   * @param ttl
   * @returns {Promise<Search>}
   */
  save(model, ttl) {
    return new Promise((resolve, reject) => {
      SearchMongo.findOneAndUpdate(
        {keyword: model.keyword},
        {$set: model, expireAt: ttl ? new Date().valueOf() + ttl : null},
        {upsert: true, new: true}
      )
        .exec()
        .then((result) => {
          if (result && result.keyword && result._id) {
            infoMessage(`Search (keyword:${result.keyword}) saved.`);
            resolve(new Search(result));
          } else {
            errorMessage(`Search (keyword:${result.keyword}) not saved.`);
            reject(new Error(`Search (keyword:${result.keyword}) not saved.`));
          }
        })
        .catch((error) => {
          errorMessage(error.message);
          reject(new Error('Search cannot be stored in DB.'));
        });
    });
  }

  /**
   *
   * @param keyword
   * @returns {Promise}
   */
  get(keyword) {
    return new Promise((resolve, reject) => {
      return SearchMongo.find({keyword: keyword}).limit(1)
        .then((obj) => {
          if (obj && obj.length > 0) {
            infoMessage(`Search (keyword:${obj[0].keyword}) loaded from db.`);
            resolve(new Search(obj[0]));
          } else {
            resolve(null);
          }
        })
        .catch((error) => {
          errorMessage(error.message);
          reject(new Error('Search result cannot be obtained from DB.'));
        });
    });
  }
}

module.exports = SearchMongoRepository;
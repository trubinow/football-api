const itemTitle = 'country';

// models
const Model = require('../../models/country');

const infoMessage = require('debug')(`${itemTitle}-mongo:info`);
const errorMessage = require('debug')(`${itemTitle}-mongo:error`);

// mongo models
const ModelMongo = require('../../mongo/models/Country');
const ModelMongoList = require('../../mongo/models/CountriesList');

class CountryMongoRepository {

  /**
   * @param model<Model>
   * @returns {Promise<Model>}
   */
  save(model) {
    return new Promise((resolve, reject) => {
      ModelMongo.findOneAndUpdate(
        {name: model.name},
        {$set: model},
        {upsert: true, new: true}
      )
        .exec()
        .then((result) => {
          if (result && result.id && result._id) {
            infoMessage(`[${itemTitle}] (id:${result.id}) saved.`);
            resolve(new Model(result));
          } else {
            errorMessage(`[${itemTitle}] (id:${result.id}) not saved.`);
            reject(new Error(`[${itemTitle}] (id:${result.id}) not saved.`));
          }
        })
        .catch((error) => {
          errorMessage(error.message);
          reject(new Error(`[${itemTitle}] cannot be stored in DB.`));
        });
    });
  }

  /**
   *
   * @param models<[Country]>
   */
  saveList(models) {
    let promiseArray = [];

    models.forEach((model) => {

      promiseArray.push(new Promise((resolve, reject) => {
        ModelMongo.findOneAndUpdate(
          {id: model.id},
          {$set: model},
          {upsert: true, new: true}
        )
          .exec()
          .then((result) => {
            if (result && result._id) {
              infoMessage(`[${itemTitle}] (id:${result.id}) saved.`);
              resolve({country: result._id});
            } else {
              errorMessage(`[${itemTitle}] (id:${result.id}) not saved.`);
              reject(new Error(`[${itemTitle}] (id:${result.id}) not saved.`));
            }
          })
          .catch((error) => {
            errorMessage(error.message);
            reject(new Error(`[${itemTitle}] list cannot be stored in DB.`));
          });
      }));
    });

    return new Promise((resolve, reject) => {
      Promise.all(promiseArray)
        .then((result) => {

          if (result && result.length > 0) {
            this._clearTop();

            new ModelMongoList({countries: result})
              .save()
              .then(() => {

                infoMessage(`[${itemTitle}] list updated`);
                resolve(true);
              });

          } else {
            resolve(false);
          }
        })
        .catch((error) => {
          errorMessage(error.message);
          reject(new Error(`[${itemTitle}] list save cannot be done.`));
        });
    });
  }

  /**
   *
   * @param id
   * @returns {Promise<Country>}
   */
  get(id) {
    return new Promise((resolve, reject) => {
      return ModelMongo.find({id: id}).limit(1)
        .then((obj) => {
          if (obj && obj.length > 0) {
            infoMessage(`[${itemTitle}] (id:${obj[0].id}) loaded from db.`);
            resolve(new Model(obj[0]));
          } else {
            resolve(null);
          }
        })
        .catch((error) => {
          errorMessage(error.message);
          reject(new Error(`[${itemTitle}] cannot be obtained from DB.`));
        });
    });
  }

  /**
   *
   * @returns {Promise<[Model]>}
   */
  getList() {

    return new Promise((resolve, reject) => {
      ModelMongoList.find({}).populate('countries.country')
        .exec()
        .then((mongoObj) => {

          if (mongoObj[0] && mongoObj[0].countries.length > 0) {
            let models = [];

            mongoObj[0].countries.forEach((obj) => {

              if (obj && obj !== []) {
                models.push(new Model(obj.country));
              }
            });

            infoMessage(`[${itemTitle}] list loaded from db.`);
            resolve(models);
          } else {
            resolve(null);
          }
        })
        .catch((error) => {
          errorMessage(error.message);
          reject(new Error(`[${itemTitle}] list cannot be obtained from DB`));
        });
    });
  }

  /**
   *
   * @returns {Promise<[Model]>}
   */
  getListAdmin(filter, pagination = {page : 1, perPage: 50}) {

    let page = pagination.page || 1;
    let perPage = pagination.perPage || 50;

    filter.countries = null;

    return new Promise((resolve, reject) => {
      ModelMongo.find(filter).skip((page - 1)*perPage).limit(perPage).sort('id')
        .exec()
        .then((results) => {

          if (results && Array.isArray(results) && results.length > 0) {
            infoMessage(`[${itemTitle}] list loaded from db.`);
            resolve(results.map((item) => new Model(item)));

          } else {
            errorMessage(`[${itemTitle}] list NOT loaded from db.`);
            resolve(null);
          }

        })
        .catch((error) => {
          errorMessage(error.message);
          reject(new Error(`[${itemTitle}] list cannot be obtained from DB`));
        });
    });
  }

  _clearTop() {
    ModelMongoList.remove({})
      .exec()
      .then(() => {
        infoMessage(`[${itemTitle}] list cleared.`);
      })
      .catch((error) => {
        errorMessage(error.message);
      });
  }

  _clearAll() {
    return new Promise((resolve, reject) => {
      ModelMongo.remove({})
        .exec()
        .then(() => {
          infoMessage(`[${itemTitle}] list cleared.`);
          resolve(true);
        })
        .catch((error) => {
          errorMessage(error.message);
          resolve(false);
        });
    });
  }

}

module.exports = CountryMongoRepository;
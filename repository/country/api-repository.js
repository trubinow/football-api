const errorMessage = require('debug')('api-country-repo:error');

const listName = 'countries';
const Model = require('../../models/country');
const ModelMapper = require('../../mappers/apifootballcom/country');

class CountryRepository {
  /**
   * @param {Api} api
   */
  constructor(api) {
    this.api = api;
  }

  getList() {
    return new Promise((resolve, reject) => {
      Promise.all([
        this.api.getList(listName),
      ])
        .then((result) => {

          if (result && result[0] && Array.isArray(result[0]) && result[0].length > 0) {

            resolve(result[0].reduce((aggr, item) => {
              let mItem = new ModelMapper(item);

              if (mItem.id) {
                aggr.push(mItem);
              }

              return aggr;

            }, []));

          } else {
            resolve([]);
          }

        })
        .catch((error) => {
          errorMessage(error.message);
          reject(new Error('The resource you requested could not be found.'));
        });
    });
  }
}

module.exports = CountryRepository;
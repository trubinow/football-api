const errorMessage = require('debug')('api-match-repo:error');
const dateFormat = require('dateformat');

const listName = 'matches';
const Model = require('../../models/match');
const ModelMapper = require('../../mappers/apifootballcom/match');

class MatchRepository {
  /**
   * @param {Api} api
   */
  constructor(api) {
    this.api = api;
  }

  getList(date = null) {

    let dateFrom = date;

    if (!date) {
      let now = new Date();
      dateFrom = dateFormat(now, 'yyyy-mm-dd');
    }

    let dateTo = dateFrom;

    return new Promise((resolve, reject) => {

      Promise.all([
        this.api.getList(listName, `&from=${dateFrom}&to=${dateTo}`),
      ])
        .then((result) => {

          if (result && result[0] && Array.isArray(result[0]) && result[0].length > 0) {

            resolve(result[0].reduce((aggr, item) => {
              let mItem = item.source != 'parser' ? new ModelMapper(item) : new Model(item);



              if (mItem.id) {
                aggr.push(mItem);
              }

              return aggr;

            }, []));

          } else {
            resolve([]);
          }

        })
        .catch((error) => {
          errorMessage(error.message);
          //reject(new Error('The resource you requested could not be found.'));
          resolve([]);
        });
    });
  }
}

module.exports = MatchRepository;
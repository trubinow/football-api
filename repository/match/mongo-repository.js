const dateFormat = require('dateformat');
const itemTitle = 'match';

const sha1 = require('sha1');

// models
const Model = require('../../models/match');
const ModelAssets = require('../../models/match-assets');

const infoMessage = require('debug')(`${itemTitle}-mongo:info`);
const errorMessage = require('debug')(`${itemTitle}-mongo:error`);

// mongo models
const ModelMongo = require('../../mongo/models/Match');
const TeamMongo = require('../../mongo/models/Team');

class MatchMongoRepository {

  /**
   *
   * @param models<[Match]>
   */
  saveList(models) {
    let promiseArray = [];

    models.forEach((model) => {

      let teamProm = new Promise((resolve) => {resolve(model.awayTeamName);});

      promiseArray.push(new Promise((resolve, reject) => {

        model.matchTimeStamp = Date.parse(`${model.date} ${model.time} UTC`) || 0;

        teamProm
          .then(this.getTeamByName)
          .then((team) => {

            if (team) {
              model.awayTeamId = team.id || '';
              model.awayImage = team.image || '';
              model.awayImageBase = team.imageBase || '';
            } else {
              model.awayImage = null;
            }

            return model.homeTeamName;
          })
          .then(this.getTeamByName)
          .then((team) => {
            if (team) {
              model.homeTeamId = team.id || '';
              model.homeImage = team.image || '';
              model.homeImageBase = team.imageBase || '';
            } else {
              model.homeImage = null;
            }

            return model;
          })
          .then(() => {

            if (model.awayImage !== null && model.homeImage !== null) {
              model.id = sha1(`${model.homeTeamName}${model.awayTeamName}${model.date}`);

              return ModelMongo.findOneAndUpdate(
                {id: model.id},
                {$set: model},
                {upsert: true, new: true}
              ).exec();
            } else {
              return false;
            }
          })
          .then((result) => {
            if (result && result._id) {
              infoMessage(`[${itemTitle}] (id:${result.id}) saved.`);
              resolve({match: result._id});
            } else {
              errorMessage(`[${itemTitle}] (id:${result.id}) not saved.`);
              resolve(false);
              //reject(new Error(`[${itemTitle}] (id:${result.id}) not saved.`));
            }
          })
          .catch((error) => {
            errorMessage(error.message);
            reject(new Error(`[${itemTitle}] list cannot be stored in DB.`));
          });

      }));
    });

    return Promise.all(promiseArray);
  }

  /**
   *
   * @returns {Promise<[Model]>}
   */
  getList(filter, pagination = {page : 1, perPage: 50}) {

    if (!filter.date) {
      let now = new Date();
      filter.date = dateFormat(now, 'yyyy-mm-dd');
    }

    let itemsPerPage = pagination.perPage;
    let page = pagination.page;

    return new Promise((resolve, reject) => {
      ModelMongo.find(filter).sort({time:1}).skip(itemsPerPage*(page-1)).limit(itemsPerPage)
        .exec()
        .then((results) => {

          if (results && Array.isArray(results) && results.length > 0) {
            infoMessage(`[${itemTitle}] list loaded from db.`);
            resolve(results.map((item) => new Model(item)));

          } else {
            errorMessage(`[${itemTitle}] list NOT loaded from db.`);
            resolve(null);
          }

        })
        .catch((error) => {
          errorMessage(error.message);
          reject(new Error(`[${itemTitle}] list cannot be obtained from DB`));
        });
    });
  }

  /**
   *
   * @returns {Promise<[Model]>}
   */
  getAssets(filter, pagination = {page : 1, perPage: 50}) {

    if (!filter.date) {
      let now = new Date();
      filter.date = dateFormat(now, 'yyyy-mm-dd');
    }

    let itemsPerPage = pagination.perPage;
    let page = pagination.page;

    return new Promise((resolve, reject) => {
      ModelMongo.find(filter).skip(itemsPerPage*(page-1)).limit(itemsPerPage)
        .exec()
        .then((results) => {

          if (results && Array.isArray(results) && results.length > 0) {
            infoMessage(`[${itemTitle}] list loaded from db.`);
            resolve(results.map((item) => new ModelAssets(item)));

          } else {
            errorMessage(`[${itemTitle}] list NOT loaded from db.`);
            resolve(null);
          }

        })
        .catch((error) => {
          errorMessage(error.message);
          reject(new Error(`[${itemTitle}] list cannot be obtained from DB`));
        });
    });
  }

  getTeamByName(teamName) {
    return new Promise((resolve, reject) => {

      //TeamMongo.find({name: teamName}).limit(1)
      TeamMongo.find({id: sha1(teamName)}).limit(1)
        .exec()
        .then((teamModelArr) => {
          if (Array.isArray(teamModelArr) && teamModelArr.length == 1 && teamModelArr[0].name) {
            infoMessage(`Image obtained: ${teamModelArr[0].name}`);
            resolve(teamModelArr[0]);
          } else {

            resolve(null);
          }

        })
        .catch((err) => {
          errorMessage(err);
          resolve(null);
        });
    });
  }


  _clearAll() {
    return new Promise((resolve, reject) => {
      resolve(true);
    });
  }

}

module.exports = MatchMongoRepository;
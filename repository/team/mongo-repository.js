const itemTitle = 'team';
const sha1 = require('sha1');

// models
const Model = require('../../models/team');
const ModelOut = require('../../models/team-out');
const ModelAdmin = require('../../models/team-admin');
const ModelAsset = require('../../models/team-assets');

const infoMessage = require('debug')(`${itemTitle}-mongo:info`);
const errorMessage = require('debug')(`${itemTitle}-mongo:error`);

// mongo models
const ModelMongo = require('../../mongo/models/Team');
//const ModelMongoList = require('../../mongo/models/CountriesList');

class TeamMongoRepository {
  /**
   * @param model<Model>
   * @returns {Promise<Model>}
   */
  save(model) {
    return new Promise((resolve, reject) => {
      ModelMongo.findOneAndUpdate(
        {id: model.id},
        {$set: model},
        {upsert: true, new: true}
      )
        .exec()
        .then((result) => {
          if (result && result.name && result._id) {
            infoMessage(`[${itemTitle}] (id:${result.name}) saved.`);
            resolve(new Model(result));
          } else {
            errorMessage(`[${itemTitle}] (id:${result.name}) not saved.`);
            reject(new Error(`[${itemTitle}] (id:${result.name}) not saved.`));
          }
        })
        .catch((error) => {
          errorMessage(error.message);
          reject(new Error(`[${itemTitle}] cannot be stored in DB.`));
        });
    });
  }

  /**
   *
   * @param models<[Team]>
   */
  saveList(models) {
    let promiseArray = [];

    models.forEach((model) => {

      promiseArray.push(new Promise((resolve, reject) => {

        model.id = sha1(model.name);

        ModelMongo.findOneAndUpdate(
          {id: model.id},
          {
            $set: {name: model.name, countryId: model.countryId, countryName: model.countryName, step: 1},
            $addToSet: {leagues: model.leagueId, aliases: model.name}
          },
          {upsert: true, new: true}
        )
          .exec()
          .then((result) => {
            if (result && result._id) {
              infoMessage(`[${itemTitle}] (id:${result.name}) saved.`);
              resolve(true);
            } else {
              errorMessage(`[${itemTitle}] (id:${result.name}) not saved.`);
              reject(new Error(`[${itemTitle}] (id:${result.name}) not saved.`));
            }
          })
          .catch((error) => {
            errorMessage(error.message);
            reject(new Error(`[${itemTitle}] list cannot be stored in DB.`));
          });
      }));

    });

    return Promise.all(promiseArray);
  }

  /**
   *
   * @returns {Promise<[Model]>}
   */
  getList(filter, pagination = {page : 1, perPage: 50}, admin = false) {

    let page = pagination.page || 1;
    let perPage = pagination.perPage || 50;

    return new Promise((resolve, reject) => {
      ModelMongo.find(filter).sort({name: 1}).skip((page - 1)*perPage).limit(perPage)
        .exec()
        .then((results) => {

          if (results && Array.isArray(results) && results.length > 0) {
            infoMessage(`[${itemTitle}] list loaded from db.`);
            if (admin) {
              resolve(results.map((item) => new ModelAdmin(item)));
            } else {
              resolve(results.map((item) => new ModelOut(item)));
            }


          } else {
            errorMessage(`[${itemTitle}] list NOT loaded from db.`);
            resolve(null);
          }

        })
        .catch((error) => {
          errorMessage(error.message);
          reject(new Error(`[${itemTitle}] list cannot be obtained from DB`));
        });
    });
  }

  /**
   *
   * @returns {Promise<[Model]>}
   */
  getAssets(filterArray, pagination = {page : 1, perPage: 600}) {

    let page = pagination.page || 1;
    let perPage = pagination.perPage || 50;

    return new Promise((resolve, reject) => {
      if (Array.isArray(filterArray) && filterArray.length > 0) {

        ModelMongo.find({id: {$in: filterArray}}).skip((page - 1) * perPage).limit(perPage)
          .exec()
          .then((results) => {

            if (results && Array.isArray(results) && results.length > 0) {

              infoMessage(`[${itemTitle}] list loaded from db.`);
              resolve(results.map((item) => new ModelAsset(item)));

            } else {
              errorMessage(`[${itemTitle}] list NOT loaded from db.`);
              resolve(null);
            }
          })
          .catch((error) => {
            errorMessage(error.message);
            reject(new Error(`[${itemTitle}] list cannot be obtained from DB`));
          });

      } else {
        resolve(null);
      }
    });
  }

  getCount(filter) {
    return new Promise((resolve) => {
      ModelMongo.count(filter)
        .exec()
        .then(count => {
          if (count) {
            resolve(count)
          } else {
            resolve(0);
          }
        })
        .catch((error) => {
          resolve(0);
        })
    })
  }

  /**
   *
   * @param id
   * @returns {Promise<Team>}
   */
  get(id) {
    return new Promise((resolve, reject) => {
      return ModelMongo.find({id: id}).limit(1)
        .then((obj) => {
          if (obj && obj.length > 0) {
            infoMessage(`[${itemTitle}] (id:${obj[0].name}) loaded from db.`);
            resolve(new Model(obj[0]));
          } else {
            resolve(null);
          }
        })
        .catch((error) => {
          errorMessage(error.message);
          reject(new Error(`[${itemTitle}] cannot be obtained from DB.`));
        });
    });
  }

  _clearAll() {
    return new Promise((resolve, reject) => {
      ModelMongo.remove({})
        .exec()
        .then(() => {
          infoMessage(`[${itemTitle}] list cleared.`);
          resolve(true);
        })
        .catch((error) => {
          errorMessage(error.message);
          resolve(false);
        });
    });
  }

}

module.exports = TeamMongoRepository;
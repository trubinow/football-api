const errorMessage = require('debug')('api-team-repo:error');
const infoMessage = require('debug')('api-team-repo:log');

const listName = 'teams';
const ModelMapper = require('../../mappers/apifootballcom/team');

class TeamRepository {
  /**
   * @param {Api} api
   */
  constructor(api) {
    this.api = api;
  }

  getList() {
    return new Promise((resolve, reject) => {

      //, '&country_id=171' - Spain
      this.api.getList('leagues')
        .then((res) => {

          let proms = res.map((item) => {
            return this.api.getTeamsList(listName, `&league_id=${item.league_id}`, {country_id: item.country_id});
          });

          Promise.all(proms)
            .then((resultArr) => {

              let teams = [];

              resultArr.forEach((result) => {

                if (result && Array.isArray(result) && result.length > 0) {

                  result.forEach((item) => {

                    let mItem = new ModelMapper(item);

                    if (mItem.name) {
                      teams.push(mItem);
                    }

                  });
                }

              });

              infoMessage(`Teams count ${teams.length}`);
              resolve(teams);

            })
            .catch((error) => {
              errorMessage(error.message);
              reject(new Error('The resource you requested could not be found.'));
            });



        })
        .catch((error) => {
          errorMessage(error);
        });


    });
  }
}

module.exports = TeamRepository;
const itemTitle = 'league';

// models
const Model = require('../../models/league');
const ChildModel = require('../../models/team-out');

const infoMessage = require('debug')(`${itemTitle}-mongo:info`);
const errorMessage = require('debug')(`${itemTitle}-mongo:error`);

// mongo models
const modelKey = 'league';
const modelListKey = 'leagues';

const ModelMongo = require('../../mongo/models/League');
const ModelMongoList = require('../../mongo/models/LeaguesList');
const ChildModelMongo = require('../../mongo/models/Team');

class LeagueMongoRepository {

  /**
   * @param model<Model>
   * @returns {Promise<Model>}
   */
  save(model) {
    return new Promise((resolve, reject) => {
      ModelMongo.findOneAndUpdate(
        {id: model.id},
        {$set: model},
        {upsert: true, new: true}
      )
        .exec()
        .then((result) => {
          if (result && result.id && result._id) {
            infoMessage(`[${itemTitle}] (id:${result.name}) saved.`);
            resolve(new Model(result));
          } else {
            errorMessage(`[${itemTitle}] (id:${result.name}) not saved.`);
            reject(new Error(`[${itemTitle}] (id:${result.name}) not saved.`));
          }
        })
        .catch((error) => {
          errorMessage(error.message);
          reject(new Error(`[${itemTitle}] cannot be stored in DB.`));
        });
    });
  }

  getPromise(model, modelType) {
    return new Promise((resolve, reject) => {

      ModelMongo.findOneAndUpdate(
        {id: model.id},
        {$set: model},
        {upsert: true, new: true}
      )
        .exec()
        .then((result) => {
          if (result && result._id) {
            infoMessage(`[${itemTitle}] (id:${result.id}) saved.`);

            let res = {};
            res[modelType] = result._id;
            resolve(res);

          } else {
            errorMessage(`[${itemTitle}] (id:${result.id}) not saved.`);
            reject(new Error(`[${itemTitle}] (id:${result.id}) not saved.`));
          }
        })
        .catch((error) => {
          errorMessage(error.message);
          reject(new Error(`[${itemTitle}] list cannot be stored in DB.`));
        });
    });
  }

  getChildModelsById(parentId) {
    return new Promise((resolve, reject) => {

      let teams = [];

      ChildModelMongo.find({leagues: parentId})
        .exec()
        .then((childModels) => {

          childModels.forEach((cmodel) => {
            teams.push({'team': cmodel._id});
          });

          resolve(teams);
        })
        .catch((err) => {
          errorMessage(err);
          resolve(teams);
        });
    });
  }


  /**
   *
   * @param models<[Country]>
   */
  saveList(models) {

    let prom = new Promise((resolve, reject) => {resolve(true);});

    models.forEach((model) => {

      prom = this.getChildModelsById(model.id).then((teams) => {

        model.teams = teams;

        return this.getPromise(model, 'league');
      });

    });

    return prom;
  }

  /**
   *
   * @param id
   * @returns {Promise<Movie>}
   */
  get(id) {
    return new Promise((resolve, reject) => {
      return ModelMongo.find({id: id}).limit(1)
        .then((obj) => {
          if (obj && obj.length > 0) {
            infoMessage(`[${itemTitle}] (id:${obj[0].id}) loaded from db.`);
            resolve(new Model(obj[0]));
          } else {
            resolve(null);
          }
        })
        .catch((error) => {
          errorMessage(error.message);
          reject(new Error(`[${itemTitle}] cannot be obtained from DB.`));
        });
    });
  }

  /**
   *
   * @param id
   * @returns {Promise<Movie>}
   */
  getTeams(id) {
    return new Promise((resolve, reject) => {
      return ModelMongo.find({id: id}).limit(1).populate('teams.team')
        .then((obj) => {
          if (obj && obj.length > 0) {
            infoMessage(`[${itemTitle}] (id:${obj[0].id}) loaded from db.`);

            let res = obj[0].teams.map((item, index) => {
              return new ChildModel(item.team);
            });

            resolve(res);
          } else {
            resolve(null);
          }
        })
        .catch((error) => {
          errorMessage(error.message);
          reject(new Error(`[${itemTitle}] cannot be obtained from DB.`));
        });
    });
  }

  /**
   *
   * @param id
   * @returns {Promise<Movie>}
   */
  getByCountryId(id) {
    return new Promise((resolve, reject) => {
      return ModelMongo.find({countryId: id})
        .then((objList) => {
          if (objList && objList.length > 0) {

            let models = [];

            objList.forEach((obj) => {
              if (obj && obj !== []) {
                models.push(new Model(obj));
              }
            });

            resolve(models);

          } else {
            resolve(null);
          }
        })
        .catch((error) => {
          errorMessage(error.message);
          reject(new Error(`[${itemTitle}] cannot be obtained from DB.`));
        });
    });
  }


  /**
   *
   * @returns {Promise<[Model]>}
   */
  getList(filter, pagination = {page : 1, perPage: 50}) {

    let page = pagination.page || 1;
    let perPage = pagination.perPage || 50;

    return new Promise((resolve, reject) => {
      ModelMongo.find(filter).skip((page - 1)*perPage).limit(perPage)
        .exec()
        .then((results) => {

          if (results && Array.isArray(results) && results.length > 0) {
            infoMessage(`[${itemTitle}] list loaded from db.`);
            resolve(results.map((item) => new Model(item)));

          } else {
            errorMessage(`[${itemTitle}] list NOT loaded from db.`);
            resolve(null);
          }

        })
        .catch((error) => {
          errorMessage(error.message);
          reject(new Error(`[${itemTitle}] list cannot be obtained from DB`));
        });
    });
  }

  _clearTop() {
    ModelMongoList.remove({})
      .exec()
      .then(() => {
        infoMessage(`[${itemTitle}] list cleared.`);
      })
      .catch((error) => {
        errorMessage(error.message);
      });
  }

  _clearAll() {
    return new Promise((resolve, reject) => {
      ModelMongo.remove({})
        .exec()
        .then(() => {
          infoMessage(`[${itemTitle}] list cleared.`);
          resolve(true);
        })
        .catch((error) => {
          errorMessage(error.message);
          resolve(false);
        });
    });
  }

}

module.exports = LeagueMongoRepository;
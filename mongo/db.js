const mongoose = require('mongoose');
const config   = require('config');

const dbConfig              = config.get('db');
const dbURI                 = dbConfig.connection + '/' + dbConfig.name;
const mongoReconnectTimeout = dbConfig.mongoReconnectTimeout;

const infoMessage  = require('debug')('db:info');
const errorMessage = require('debug')('db:error');

let connectedState = 0;

module.exports = function () {
  mongoose.Promise = global.Promise;

  function connectWithRetry() {
    return mongoose.connect(dbURI, {useNewUrlParser: true, autoReconnect: false}, function (err) {
      if (err) {
        errorMessage('Failed to connect to mongo on startup - retrying in 5 sec : ' +  + dbURI);
        setTimeout(connectWithRetry, mongoReconnectTimeout);
      } else {
        connectedState = 1;
        infoMessage('MongoDB connected to ' + dbURI);
      }
    });
  }

  mongoose.connection.on('disconnected', () => {
    if (connectedState) {
      connectedState = 0;
      errorMessage('MongoDB disconnected error');
      connectWithRetry();
    }
  });

  process.on('SIGINT', () => {
    mongoose.connection.close(function () {
      errorMessage('Force to close the MongoDB conection');
      process.exit(0);
    });
  });

  process.on('SIGINT2', () => {
    mongoose.connection.close(function () {
      errorMessage('Force to close the MongoDB conection');
      process.kill(process.pid, 'SIGUSR2');
    });
  });

  process.on('unhandledRejection', function (err) {
  });

  if (!mongoose.connection.readyState) {
    connectWithRetry();
  }
};

module.exports.errorOnDisconnect = function (req, res, next) {
  if (mongoose.connection.readyState !== 1) {
    return res.status(500)
      .json({
        errors: 'No connection to database',
      });
  }

  return next();
};

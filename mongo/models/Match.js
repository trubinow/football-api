const mongoose = require('mongoose');

const matchSchema = new mongoose.Schema(
  {
    id: {type: String, unique: true, required: true},
    countryId : {type: String, default: ''},
    countryName : {type: String, default: ''},
    leagueId : {type: String, default: ''},
    leagueName : {type: String, default: ''},

    date: {type: String, default: ''},
    time: {type: String, default: ''},
    status : {type: String, default: '0'},
    image: {type: String, default: ''},
    live: {type: Boolean, default: false},

    homeTeamId: {type: String, default: ''},
    awayTeamId: {type: String, default: ''},

    homeTeamName : {type: String, default: ''},
    homeTeamScore : {type: String, default: '0'},

    awayTeamName : {type: String, default: ''},
    awayTeamScore : {type: String, default: ''},

    homeImage: {type: String, default: ''},
    awayImage: {type: String, default: ''},

    homeImageBase: {type: String, default: ''},
    awayImageBase: {type: String, default: ''},

    matchTimeStamp: {type: String, default: '0'},

    goalScorer : [],
    cards: [],
    lineup: [],
    statistics: []
  },
  {
    timestamps: true
  }
);

matchSchema.index({date : 1});

module.exports = mongoose.model('Match', matchSchema);
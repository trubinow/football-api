const mongoose = require('mongoose');

const countriesList = new mongoose.Schema(
  {
    name: String,
    countries: [{
      country: {type: mongoose.Schema.Types.ObjectId, ref: 'Country'}
    }]
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model('countriesList', countriesList);
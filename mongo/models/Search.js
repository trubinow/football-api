const mongoose = require('mongoose');

const searchSchema = new mongoose.Schema(
  {
    keyword   : {type: String, unique: true, required: true},
    movies    : {type: Array, default: []},
    persons   : {type: Array, default: []}
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model('Search', searchSchema);
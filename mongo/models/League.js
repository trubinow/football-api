const mongoose = require('mongoose');

const leagueSchema = new mongoose.Schema(
  {
    id: {type: String, unique: true, required: true},
    name: {type: String, default: ''},
    image: {type: String, default: ''},
    countryId : {type: String, default: ''},
    countryName : {type: String, default: ''},
    teams: [{
      team: {type: mongoose.Schema.Types.ObjectId, ref: 'Team'}
    }]
  },
  {
    timestamps: true
  }
);

leagueSchema.index({countryId : 1});

module.exports = mongoose.model('League', leagueSchema);
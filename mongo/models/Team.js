const mongoose = require('mongoose');

const teamSchema = new mongoose.Schema(
  {
    id: {type: String, unique: true, required: true},
    name: {type: String, unique: true, required: true},
    aliases: [{type: String, default: ''}],
    leagues: [{type: String, default: ''}],
    countryId: {type: String, default: ''},
    countryName: {type: String, default: ''},
    image: {type: String, default: ''},
    imageBase: {type: String, default: ''},
    sourcePage: {type: String, default: ''},
    sourceMask: {type: String, default: ''},
    step: {type: String, default: '1'}
  },
  {
    timestamps: true
  }
);

teamSchema.index({'name' : 1});

module.exports = mongoose.model('Team', teamSchema);
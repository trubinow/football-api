const mongoose = require('mongoose');

const leaguesList = new mongoose.Schema(
  {
    name: String,
    leagues: [{
      league: {type: mongoose.Schema.Types.ObjectId, ref: 'League'}
    }]
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model('leagues', leaguesList);
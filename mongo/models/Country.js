const mongoose = require('mongoose');

const countrySchema = new mongoose.Schema(
  {
    id: {type: String, unique: true, required: true},
    name: {type: String, default: ''},
    image: {type: String, default: ''},
  },
  {
    timestamps: true
  }
);

countrySchema.path('name').index({ unique: true });
module.exports = mongoose.model('Country', countrySchema);
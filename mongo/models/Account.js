const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const uniqueValidator = require('mongoose-unique-validator');

const accountSchema = new mongoose.Schema(
  {
    username: {type: String, unique: true, required: true},
    email: {type: String, default: ""},
    ip: {type: String, default: ''},
    atype: {type: String, default: '0'},
    active: {type: String, default: '0'},
    hash: String,
    salt: String
  },
  {
    timestamps: true
  }
);

accountSchema.plugin(uniqueValidator, {message: 'is already taken.'});

accountSchema.methods.setPassword = function (password) {
  this.salt = crypto.randomBytes(16).toString('base64');
  this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha1').toString('base64');
}

accountSchema.methods.validPassword = function (password) {
  let hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha1').toString('base64');
  return this.hash === hash;
};

accountSchema.methods.isActive = function () {
  return this.active && this.active === '1';
};

accountSchema.methods.generateJwt = function () {
  const expiry = new Date();
  expiry.setDate(expiry.getDate() + 1);

  return jwt.sign({
    _id: this._id,
    username: this.username,
    exp: parseInt(expiry.getTime() / 1000)
  }, process.env.JWT_SECRET);

}

module.exports = mongoose.model('Account', accountSchema);
const config = require('config');
const infoMsg = require('debug')('wikipedia-provider:info');
const errorMsg = require('debug')('wikipedia-provider:error');

const wikiUrl = config.get('apis.wikipediaorg.host') + '/wiki/';
const wikiList = config.get('apis.wikipediaorg.list');

/*Teams*/

let parseStepOne =  function(item) {

  let key = item.name;
  let url = '';

  if (/^FC /i.test(key) || / FC$/i.test(key) || / FC /i.test(key)
    || /^FK /i.test(key) || / FK$/i.test(key) || / FK /i.test(key)) {

    url = wikiUrl + key;

  } else if (/U17$/i.test(key) || /U19$/i.test(key) || /U20$/i.test(key) || /U21$/i.test(key) || /U23$/i.test(key)) {

    key = key
      .replace('U17', '_national_football_team')
      .replace('U19', '_national_football_team')
      .replace('U20', '_national_football_team')
      .replace('U21', '_national_football_team')
      .replace('U23', '_national_football_team')

    url = wikiUrl + key;

  } else {

    if (key.split(' ').length > 1) {
      url = wikiUrl + key;
    } else{
      url = wikiUrl + key + '_F.C.';
    }
  }

  return url;
}

let parseStepTwo = function(item) {

  let key = item.name;

  if (/^FC /i.test(key) || / FC$/i.test(key) || / FC /i.test(key)
    || /^FK /i.test(key) || / FK$/i.test(key) || / FK /i.test(key)) {

    key = key
      .replace('FK', '')
      .replace('Fk', '')
      .replace('FC', '')
      .replace('Fc', '')
      .replace('fk', '');
  }

  return wikiUrl + key;
}

let parseStepThree = function(item) {
  let key = item.name;
  return wikiList.replace('%subject%', key);
}

let parseStepFour = function(item) {
  let key = item.name;
  return wikiList.replace('%subject%', key + ' club');
}

/*Countries*/
let parseCountries = function(key) {
  return wikiUrl + key;
}

/*Countries*/
let parseLeagues = function(key) {
  return wikiUrl + key;
}

module.exports = {
  stepOne: parseStepOne,
  stepTwo: parseStepTwo,
  stepThree: parseStepThree,
  stepFour: parseStepFour,
  countries: parseCountries,
  leagues: parseLeagues
};